package com.ruoyi.serve.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 服务记录对象 t_customer_serve
 *
 * @author ruoyi
 * @date 2022-11-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_customer_serve")
public class CustomerServe extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 服务记录ID
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 服务人员ID
     */
    private Long userId;
    /**
     * 客户ID
     */
    private Long customerId;
    /**
     * 开始时间
     */
    private Date beginTime;
    /**
     * 结束时间
     */
    private Date endTime;
    /**
     * 开始前照片
     */
    private String beforeImgs;
    /**
     * 开始后照片
     */
    private String afterImgs;
    /**
     * 开始前视频
     */
    private String beforeVideo;
    /**
     * 开始后视频
     */
    private String afterVideo;
    /**
     * 服务状态，见字典：0=未开始；1=进行中；2=已完成；
     */
    private String status;

}
