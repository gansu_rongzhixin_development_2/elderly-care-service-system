package com.ruoyi.serve.domain.bo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 任务明细业务对象 t_serve_task_item
 *
 * @author ruoyi
 * @date 2022-11-03
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class ServeTaskItemBo extends BaseEntity {

    /**
     * ID
     */
    @NotNull(message = "ID不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 任务ID
     */
    @NotNull(message = "任务ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long taskId;

    /**
     * 服务项目ID
     */
    @NotNull(message = "服务项目ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long serviceItemId;

    /**
     * 任务状态，见字典：0=未开始；1=进行中；2=已完成，3=已超期；
     */
    private String status;



    /**
     * 开始前照片，多张逗号分割
     */
    private String beforeImgs;

    /**
     * 开始前照片，多张逗号分割
     */
    private String afterImgs;

    /**
     * 签到时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date signTime;

    /**
     * 完成时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date finishTime;

    /**
     * 服务评价
     */
    private String evaluate;
}
