package com.ruoyi.serve.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.ExcelDictFormat;
import com.ruoyi.common.convert.ExcelDictConvert;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * 任务明细视图对象 t_serve_task_item
 *
 * @author ruoyi
 * @date 2022-11-03
 */
@Data
@ExcelIgnoreUnannotated
public class ServeTaskItemVo {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @ExcelProperty(value = "ID")
    private Long id;

    /**
     * 任务ID
     */
    @ExcelProperty(value = "任务ID")
    private Long taskId;

    /**
     * 服务项目ID
     */
    @ExcelProperty(value = "服务项目ID")
    private Long serviceItemId;

    /**
     * 服务项目名称
     */
    private String serviceItemName;



    /**
     * 开始前照片，多张逗号分割
     */
    private String beforeImgs;

    /**
     * 开始前照片，多张逗号分割
     */
    private String afterImgs;

    /**
     * 任务状态，见字典：0=未开始；1=进行中；2=已完成，3=已超期；
     */
    private String status;

    /**
     * 签到时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date signTime;

    /**
     * 完成时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date finishTime;


    /**
     * 服务评价
     */
    private String evaluate;

    private List<String> beforeImgList = new ArrayList<String>();

    private List<String> afterImgList = new ArrayList<String>();

    /**
     * 服务时长
     * */
    private Long serveDuration;

}
