package com.ruoyi.serve.domain.bo;

import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 服务记录业务对象 t_customer_serve
 *
 * @author ruoyi
 * @date 2022-11-02
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CustomerServeBo extends BaseEntity {

    /**
     * 服务记录ID
     */
    @NotNull(message = "服务记录ID不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 服务人员ID
     */
    @NotNull(message = "服务人员ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long userId;

    /**
     * 客户ID
     */
    @NotNull(message = "客户ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long customerId;

    /**
     * 开始时间
     */
    private Date beginTime;

    /**
     * 结束时间
     */
    private Date endTime;

    /**
     * 开始前照片
     */
    private String beforeImgs;

    /**
     * 开始后照片
     */
    private String afterImgs;

    /**
     * 开始前视频
     */
    private String beforeVideo;

    /**
     * 开始后视频
     */
    private String afterVideo;

    /**
     * 服务状态，见字典：0=未开始；1=进行中；2=已完成；
     */
    @NotBlank(message = "服务状态，见字典：0=未开始；1=进行中；2=已完成；不能为空", groups = { AddGroup.class, EditGroup.class })
    private String status;


}
