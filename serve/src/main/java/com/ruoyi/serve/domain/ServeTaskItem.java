package com.ruoyi.serve.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 任务明细对象 t_serve_task_item
 *
 * @author ruoyi
 * @date 2022-11-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_serve_task_item")
public class ServeTaskItem extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * ID
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 任务ID
     */
    private Long taskId;
    /**
     * 服务项目ID
     */
    private Long serviceItemId;

    /**
     * 开始前照片，多张逗号分割
     */
    private String beforeImgs;

    /**
     * 开始前照片，多张逗号分割
     */
    private String afterImgs;

    /**
     * 任务状态，见字典：0=未开始；1=进行中；2=已完成，3=已超期；
     */
    private String status;

    /**
     * 签到时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date signTime;

    /**
     * 完成时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date finishTime;

    /**
     * 服务评价
     */
    private String evaluate;


}
