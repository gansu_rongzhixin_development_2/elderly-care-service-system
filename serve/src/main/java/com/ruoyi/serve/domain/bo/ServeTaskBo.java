package com.ruoyi.serve.domain.bo;

import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 服务任务业务对象 t_serve_task
 *
 * @author ruoyi
 * @date 2022-11-03
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class ServeTaskBo extends BaseEntity {

    /**
     *
     */
    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 服务人员ID
     */
    @NotNull(message = "服务人员ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long userId;

    /**
     * 客户ID
     */
    @NotNull(message = "客户ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long customerId;

    /**
     * 任务起始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @NotNull(message = "任务起始时间不能为空", groups = { AddGroup.class, EditGroup.class })
    private Date beginTime;

    /**
     * 任务截止时间
     */


    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @NotNull(message = "任务截止时间不能为空", groups = { AddGroup.class, EditGroup.class })
    private Date endTime;

    /**
     * 任务状态
     */
    @NotBlank(message = "任务状态不能为空", groups = { AddGroup.class, EditGroup.class })
    private String status;

    /**
     * 员工手机
     */
    private String userPhonenumber;


    /**
     * 员工姓名
     */
    private String userName;

    /**
     * 顾客姓名
     */
    private String customerName;

    /**
     * 顾客照片
     */
    private String customerImg;


    /**
     * 街道ID
     */
    private String deptId;

    /**
     * 员工姓名
     */
    private String nickName;

    /**
     * 服务项目id集合
     */
    private Long[] serviceItemIds;



}
