package com.ruoyi.serve.domain.bo;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 员工服务对象统计
 *
 * @author ruoyi
 * @date 2022-11-02
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class UserCustomerBo extends BaseEntity {

    /**
     * 员工ID
      */
  private Long id ;

    /**
     * 员工姓名
     */
  private String name;


    /**
     * 员工头像
     */
  private String avatar;

    /**
     * 员工手机
     */
    private String phonenumber;

    /**
     * 老人数量
     */
  private Integer customerCount;




}
