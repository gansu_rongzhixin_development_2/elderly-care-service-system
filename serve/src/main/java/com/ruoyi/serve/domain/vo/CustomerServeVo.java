package com.ruoyi.serve.domain.vo;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.annotation.ExcelDictFormat;
import com.ruoyi.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 服务记录视图对象 t_customer_serve
 *
 * @author ruoyi
 * @date 2022-11-02
 */
@Data
@ExcelIgnoreUnannotated
public class CustomerServeVo {

    private static final long serialVersionUID = 1L;

    /**
     * 服务记录ID
     */
    @ExcelProperty(value = "服务记录ID")
    private Long id;

    /**
     * 服务人员ID
     */
    @ExcelProperty(value = "服务人员ID")
    private Long userId;

    /**
     * 客户ID
     */
    @ExcelProperty(value = "客户ID")
    private Long customerId;

    /**
     * 开始时间
     */
    @ExcelProperty(value = "开始时间")
    private Date beginTime;

    /**
     * 结束时间
     */
    @ExcelProperty(value = "结束时间")
    private Date endTime;

    /**
     * 开始前照片
     */
    @ExcelProperty(value = "开始前照片")
    private String beforeImgs;

    /**
     * 开始后照片
     */
    @ExcelProperty(value = "开始后照片")
    private String afterImgs;

    /**
     * 开始前视频
     */
    @ExcelProperty(value = "开始前视频")
    private String beforeVideo;

    /**
     * 开始后视频
     */
    @ExcelProperty(value = "开始后视频")
    private String afterVideo;

    /**
     * 服务状态，见字典：0=未开始；1=进行中；2=已完成；
     */
    @ExcelProperty(value = "服务状态，见字典：0=未开始；1=进行中；2=已完成；")
    private String status;

    /**
     * 记录创建时间
     */
    @ExcelProperty(value = "记录创建时间")
    private Date createTime;


}
