package com.ruoyi.serve.domain.bo;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 员工服务对象详情
 *
 * @author ruoyi
 * @date 2022-11-02
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class UserCustomerDetailBo extends BaseEntity {
    /**
     * 员工ID
     */
    private Long userId;

    /**
     * 老人ID
     */
    private Long customerId ;

    /**
     * 老人姓名
     */
    private String customerName;

    /**
     * 老人头像
     */
    private String customerAvatar;

    /**
     * 老人电话
     */
    private String customerPhonenumber;

    /**
     * 老人住址
     */
    private String customerAddress;

}
