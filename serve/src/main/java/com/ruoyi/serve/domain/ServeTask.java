package com.ruoyi.serve.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 服务任务对象 t_serve_task
 *
 * @author ruoyi
 * @date 2022-11-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_serve_task")
public class ServeTask extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     *
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 服务人员ID
     */
    private Long userId;
    /**
     * 客户ID
     */
    private Long customerId;
    /**
     * 任务起始时间
     */
    private Date beginTime;
    /**
     * 任务截止时间
     */
    private Date endTime;
    /**
     * 任务状态
     */
    private String status;
    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableLogic
    private String delFlag;

    /**
     * 员工姓名
     */
    private String userName;

    /**
     * 顾客姓名
     */
    private String customerName;

    /**
     * 顾客照片
     */
    private String customerImg;


}
