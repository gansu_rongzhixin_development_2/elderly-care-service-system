package com.ruoyi.serve.domain.vo;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.base.domain.vo.CustomerVo;
import com.ruoyi.common.annotation.ExcelDictFormat;
import com.ruoyi.common.convert.ExcelDictConvert;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;


/**
 * 服务任务视图对象 t_serve_task
 *
 * @author ruoyi
 * @date 2022-11-03
 */
@Data
@ExcelIgnoreUnannotated
public class ServeTaskVo {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     * 服务人员ID
     */
    @ExcelProperty(value = "服务人员ID")
    private Long userId;

    /**
     * 员工姓名
     */
    private String userName;

    /**
     * 客户ID
     */
    @ExcelProperty(value = "客户ID")
    private Long customerId;


    /**
     * 任务起始时间
     */
    @ExcelProperty(value = "任务起始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date beginTime;

    /**
     * 任务截止时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ExcelProperty(value = "任务截止时间")
    private Date endTime;

    /**
     * 任务状态0=未开始；1=进行中；2=已完成，3=已超期；
     */
    @ExcelProperty(value = "任务状态", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "serve_task_status")
    private String status;

    /**
     * 创建者
     */
    @ExcelProperty(value = "创建者")
    private String createBy;

    /**
     * 创建时间
     */
    @ExcelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 服务项目
     */
    private List<ServeTaskItemVo> serveTaskItemVoList;

    /**
     * 老人
     */
    private CustomerVo customer;




    /**
     * 顾客姓名
     */
    private String customerName;

    /**
     * 顾客照片
     */
    private String customerImg;

    /**
     * 客户住址
     */
    private String customerAddress;

    /**
     * 上次服务时间
     */
    @JsonFormat(pattern = "MM月dd日")
    private Date lastServeTime;

    /**
     * 员工姓名
     */
    private String nickName;
}
