package com.ruoyi.serve.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.util.Date;


/**
 * 服务记录视图对象 t_customer_serve
 *
 * @author ruoyi
 * @date 2022-11-02
 */
@Data
@ExcelIgnoreUnannotated
public class UserCustomerVo {

    private static final long serialVersionUID = 1L;

    /**
     * 员工ID
     */
    private Long id ;

    /**
     * 员工姓名
     */
    private String name;


    /**
     * 员工头像
     */
    private String avatar;

    /**
     * 员工手机
     */
    private String phonenumber;

    /**
     * 老人数量
     */
    private Integer customerCount;


}
