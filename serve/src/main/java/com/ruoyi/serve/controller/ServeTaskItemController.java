package com.ruoyi.serve.controller;

import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.validate.QueryGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.serve.domain.vo.ServeTaskItemVo;
import com.ruoyi.serve.domain.bo.ServeTaskItemBo;
import com.ruoyi.serve.service.IServeTaskItemService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 任务明细
 *
 * @author ruoyi
 * @date 2022-11-03
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/serve/serveTaskItem")
public class ServeTaskItemController extends BaseController {

    private final IServeTaskItemService iServeTaskItemService;

    /**
     * 查询任务明细列表
     */
    @SaCheckPermission("serve:serveTaskItem:list")
    @GetMapping("/list")
    public TableDataInfo<ServeTaskItemVo> list(ServeTaskItemBo bo, PageQuery pageQuery) {
        return iServeTaskItemService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出任务明细列表
     */
    @SaCheckPermission("serve:serveTaskItem:export")
    @Log(title = "任务明细", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(ServeTaskItemBo bo, HttpServletResponse response) {
        List<ServeTaskItemVo> list = iServeTaskItemService.queryList(bo);
        ExcelUtil.exportExcel(list, "任务明细", ServeTaskItemVo.class, response);
    }

    /**
     * 获取任务明细详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("serve:serveTaskItem:query")
    @GetMapping("/{id}")
    public R<ServeTaskItemVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iServeTaskItemService.queryById(id));
    }

    /**
     * 新增任务明细
     */
    @SaCheckPermission("serve:serveTaskItem:add")
    @Log(title = "任务明细", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody ServeTaskItemBo bo) {
        return toAjax(iServeTaskItemService.insertByBo(bo) ? 1 : 0);
    }

    /**
     * 修改任务明细
     */
    @SaCheckPermission("serve:serveTaskItem:edit")
    @Log(title = "任务明细", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody ServeTaskItemBo bo) {
        ServeTaskItemVo raw = iServeTaskItemService.queryById(bo.getId());
        bo.setBeforeImgs(raw.getBeforeImgs());
        bo.setAfterImgs(raw.getAfterImgs());
        return toAjax(iServeTaskItemService.updateByBo(bo) ? 1 : 0);
    }

    /**
     * 删除任务明细
     *
     * @param ids 主键串
     */
    @SaCheckPermission("serve:serveTaskItem:remove")
    @Log(title = "任务明细", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iServeTaskItemService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }
}
