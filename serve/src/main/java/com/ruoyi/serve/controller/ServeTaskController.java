package com.ruoyi.serve.controller;

import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.validate.QueryGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.serve.domain.vo.ServeTaskVo;
import com.ruoyi.serve.domain.bo.ServeTaskBo;
import com.ruoyi.serve.service.IServeTaskService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 服务任务
 *
 * @author ruoyi
 * @date 2022-11-03
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/serve/serveTask")
public class ServeTaskController extends BaseController {

    private final IServeTaskService iServeTaskService;

    /**
     * 查询服务任务列表
     */
    @SaCheckPermission("serve:serveTask:list")
    @GetMapping("/list")
    public TableDataInfo<ServeTaskVo> list(ServeTaskBo bo, PageQuery pageQuery) {
        return iServeTaskService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出服务任务列表
     */
    @SaCheckPermission("serve:serveTask:export")
    @Log(title = "服务任务", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(ServeTaskBo bo, HttpServletResponse response) {
        List<ServeTaskVo> list = iServeTaskService.queryList(bo);
        ExcelUtil.exportExcel(list, "服务任务", ServeTaskVo.class, response);
    }

    /**
     * 获取服务任务详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("serve:serveTask:query")
    @GetMapping("/{id}")
    public R<ServeTaskVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iServeTaskService.queryById(id));
    }

    /**
     * 新增服务任务
     */
    @SaCheckPermission("serve:serveTask:add")
    @Log(title = "服务任务", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody ServeTaskBo bo) {
        return toAjax(iServeTaskService.insertByBo(bo) ? 1 : 0);
    }

    /**
     * 修改服务任务
     */
    @SaCheckPermission("serve:serveTask:edit")
    @Log(title = "服务任务", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody ServeTaskBo bo) {
        return toAjax(iServeTaskService.updateByBo(bo) ? 1 : 0);
    }

    /**
     * 删除服务任务
     *
     * @param ids 主键串
     */
    @SaCheckPermission("serve:serveTask:remove")
    @Log(title = "服务任务", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iServeTaskService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }

    @GetMapping("/addLifaTask")
    public R<Void> addLifaTask() {
        return toAjax(iServeTaskService.insertLifaTask() ? 1 : 0);
    }
}
