package com.ruoyi.serve.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.ruoyi.base.service.ICustomerService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.serve.domain.bo.UserCustomerBo;
import com.ruoyi.serve.domain.bo.UserCustomerDetailBo;
import com.ruoyi.serve.domain.vo.UserCustomerDetailVo;
import com.ruoyi.serve.domain.vo.UserCustomerVo;
import com.ruoyi.serve.service.IUserCustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import java.util.Arrays;

/**
 * 员工服务人数
 *
 * @author ruoyi
 * @date 2022-11-02
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/serve/userCustomer")
public class UserCustomerController extends BaseController {

    private final IUserCustomerService userCustomerService;


    private final ICustomerService customerService;

    /**
     * 查询员工服务人数列表
     */
    @GetMapping("/list")
    public TableDataInfo<UserCustomerVo> list(UserCustomerBo bo, PageQuery pageQuery) {
        return userCustomerService.queryPageList(bo, pageQuery);
    }


    /**
     * 查询员工服务老人列表
     */
    @GetMapping("/detail/list")
    public TableDataInfo<UserCustomerDetailVo> list(UserCustomerDetailBo bo, PageQuery pageQuery) {
        return userCustomerService.queryDetailPageList(bo, pageQuery);
    }

    /**
     * 删除绑定的老人
     *
     * @param ids 主键串
     */
    @SaCheckPermission("serve:customerServe:remove")
    @Log(title = "服务记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        if(ids!=null && ids.length>0){
            customerService.updateUserId(null,ids);
        }
        return toAjax(1);
    }


}
