package com.ruoyi.serve.controller;

import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.validate.QueryGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.serve.domain.vo.CustomerServeVo;
import com.ruoyi.serve.domain.bo.CustomerServeBo;
import com.ruoyi.serve.service.ICustomerServeService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 服务记录
 *
 * @author ruoyi
 * @date 2022-11-02
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/serve/customerServe")
public class CustomerServeController extends BaseController {

    private final ICustomerServeService iCustomerServeService;

    /**
     * 查询服务记录列表
     */
    @SaCheckPermission("serve:customerServe:list")
    @GetMapping("/list")
    public TableDataInfo<CustomerServeVo> list(CustomerServeBo bo, PageQuery pageQuery) {
        return iCustomerServeService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出服务记录列表
     */
    @SaCheckPermission("serve:customerServe:export")
    @Log(title = "服务记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CustomerServeBo bo, HttpServletResponse response) {
        List<CustomerServeVo> list = iCustomerServeService.queryList(bo);
        ExcelUtil.exportExcel(list, "服务记录", CustomerServeVo.class, response);
    }

    /**
     * 获取服务记录详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("serve:customerServe:query")
    @GetMapping("/{id}")
    public R<CustomerServeVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iCustomerServeService.queryById(id));
    }

    /**
     * 新增服务记录
     */
    @SaCheckPermission("serve:customerServe:add")
    @Log(title = "服务记录", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CustomerServeBo bo) {
        return toAjax(iCustomerServeService.insertByBo(bo) ? 1 : 0);
    }

    /**
     * 修改服务记录
     */
    @SaCheckPermission("serve:customerServe:edit")
    @Log(title = "服务记录", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CustomerServeBo bo) {
        return toAjax(iCustomerServeService.updateByBo(bo) ? 1 : 0);
    }

    /**
     * 删除服务记录
     *
     * @param ids 主键串
     */
    @SaCheckPermission("serve:customerServe:remove")
    @Log(title = "服务记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iCustomerServeService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }
}
