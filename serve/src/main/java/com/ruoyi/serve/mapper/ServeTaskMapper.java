package com.ruoyi.serve.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.serve.domain.ServeTask;
import com.ruoyi.serve.domain.bo.ServeTaskBo;
import com.ruoyi.serve.domain.vo.ServeTaskVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;
import com.ruoyi.serve.domain.vo.UserCustomerVo;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * 服务任务Mapper接口
 *
 * @author ruoyi
 * @date 2022-11-03
 */
public interface ServeTaskMapper extends BaseMapperPlus<ServeTaskMapper, ServeTask, ServeTaskVo> {
    /**
     * 查询上一次服务时间
     * @param taskId
     * @return
     */
    Date getLastServeTime(Long taskId);

    Page<ServeTaskVo> selectTaskList(Page<ServeTaskVo> page, @Param("serveTaskBo") ServeTaskBo bo);

    List<ServeTaskVo> queryList(@Param("serveTaskBo") ServeTaskBo bo);
}
