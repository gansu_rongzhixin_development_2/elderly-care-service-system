package com.ruoyi.serve.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.mapper.BaseMapperPlus;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.serve.domain.CustomerServe;
import com.ruoyi.serve.domain.bo.UserCustomerBo;
import com.ruoyi.serve.domain.bo.UserCustomerDetailBo;
import com.ruoyi.serve.domain.vo.CustomerServeVo;
import com.ruoyi.serve.domain.vo.UserCustomerDetailVo;
import com.ruoyi.serve.domain.vo.UserCustomerVo;
import org.apache.ibatis.annotations.Param;

/**
 * 服务记录Mapper接口
 *
 * @author ruoyi
 * @date 2022-11-02
 */
public interface UserCustomerMapper extends BaseMapper<UserCustomerVo> {


    Page<UserCustomerVo> queryPageList(Page<UserCustomerVo> page, UserCustomerBo bo);



    /**
     * 根据员工ID查询服务老人列表
     * @param build
     * @param bo
     * @return
     */
    Page<UserCustomerDetailVo> queryDetailPageListByUserId(Page<Object> build, @Param("userId") Long userId);
}
