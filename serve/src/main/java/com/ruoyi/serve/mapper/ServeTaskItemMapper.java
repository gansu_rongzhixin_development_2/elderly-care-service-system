package com.ruoyi.serve.mapper;

import com.ruoyi.serve.domain.ServeTaskItem;
import com.ruoyi.serve.domain.vo.ServeTaskItemVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;

import java.util.List;
import java.util.Map;

/**
 * 任务明细Mapper接口
 *
 * @author ruoyi
 * @date 2022-11-03
 */
public interface ServeTaskItemMapper extends BaseMapperPlus<ServeTaskItemMapper, ServeTaskItem, ServeTaskItemVo> {

    List<Map<String, Object>> groupByServiceItemId();
}
