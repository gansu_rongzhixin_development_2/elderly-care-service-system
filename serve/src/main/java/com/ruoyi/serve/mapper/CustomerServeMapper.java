package com.ruoyi.serve.mapper;

import com.ruoyi.serve.domain.CustomerServe;
import com.ruoyi.serve.domain.vo.CustomerServeVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;

/**
 * 服务记录Mapper接口
 *
 * @author ruoyi
 * @date 2022-11-02
 */
public interface CustomerServeMapper extends BaseMapperPlus<CustomerServeMapper, CustomerServe, CustomerServeVo> {

}
