package com.ruoyi.serve.service;

import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.serve.domain.bo.CustomerServeBo;
import com.ruoyi.serve.domain.bo.UserCustomerBo;
import com.ruoyi.serve.domain.bo.UserCustomerDetailBo;
import com.ruoyi.serve.domain.vo.CustomerServeVo;
import com.ruoyi.serve.domain.vo.UserCustomerDetailVo;
import com.ruoyi.serve.domain.vo.UserCustomerVo;

import java.util.Collection;
import java.util.List;

/**
 * 服务记录Service接口
 *
 * @author ruoyi
 * @date 2022-11-02
 */
public interface IUserCustomerService {


    /**
     * 查询员工所服务人数
     */
    TableDataInfo<UserCustomerVo> queryPageList(UserCustomerBo bo, PageQuery pageQuery);


    /**
     * 查询员工所服务老人列表
     */
    TableDataInfo<UserCustomerDetailVo> queryDetailPageList(UserCustomerDetailBo bo, PageQuery pageQuery);


}
