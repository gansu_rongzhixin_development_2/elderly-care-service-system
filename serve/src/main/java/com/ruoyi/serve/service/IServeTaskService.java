package com.ruoyi.serve.service;

import com.ruoyi.serve.domain.ServeTask;
import com.ruoyi.serve.domain.vo.ServeTaskVo;
import com.ruoyi.serve.domain.bo.ServeTaskBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 服务任务Service接口
 *
 * @author ruoyi
 * @date 2022-11-03
 */
public interface IServeTaskService {

    /**
     * 查询服务任务
     */
    ServeTaskVo queryById(Long id);

    /**
     * 查询服务任务列表
     */
    TableDataInfo<ServeTaskVo> queryPageList(ServeTaskBo bo, PageQuery pageQuery);

    /**
     * 查询服务任务列表
     */
    List<ServeTaskVo> queryList(ServeTaskBo bo);

    /**
     * 新增服务任务
     */
    Boolean insertByBo(ServeTaskBo bo);

    /**
     * 修改服务任务
     */
    Boolean updateByBo(ServeTaskBo bo);

    /**
     * 校验并批量删除服务任务信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * 插入月度服务任务
     * @return
     */
    void insertMonthTask(String month);

    /**
     * 查询服务次数
     * @return
     */
    int getServiceCount();

    /**
     * 添加理发任务
     * @return
     */
    boolean insertLifaTask( );

    /**
     * 获取某月派单已服务人数
     * @param month
     * @return
     */
    int getMonthServicedCount(String month);

    /**
     * 获取某月派单未服务人数
     * @param month
     * @return
     */
    int getMonthUnservicedCount(String month);
}
