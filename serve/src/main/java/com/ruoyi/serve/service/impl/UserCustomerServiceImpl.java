package com.ruoyi.serve.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.base.mapper.CustomerMapper;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.serve.domain.CustomerServe;
import com.ruoyi.serve.domain.bo.CustomerServeBo;
import com.ruoyi.serve.domain.bo.UserCustomerBo;
import com.ruoyi.serve.domain.bo.UserCustomerDetailBo;
import com.ruoyi.serve.domain.vo.CustomerServeVo;
import com.ruoyi.serve.domain.vo.UserCustomerDetailVo;
import com.ruoyi.serve.domain.vo.UserCustomerVo;
import com.ruoyi.serve.mapper.CustomerServeMapper;
import com.ruoyi.serve.mapper.UserCustomerMapper;
import com.ruoyi.serve.service.ICustomerServeService;
import com.ruoyi.serve.service.IUserCustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 服务记录Service业务层处理
 *
 * @author ruoyi
 * @date 2022-11-02
 */
@RequiredArgsConstructor
@Service
public class UserCustomerServiceImpl implements IUserCustomerService {

    private final UserCustomerMapper baseMapper;


    @Override
    public TableDataInfo<UserCustomerVo> queryPageList(UserCustomerBo bo, PageQuery pageQuery) {
        Page<UserCustomerVo> result = baseMapper.queryPageList(pageQuery.build(),bo);
        return TableDataInfo.build(result);
    }

    @Override
    public TableDataInfo<UserCustomerDetailVo> queryDetailPageList(UserCustomerDetailBo bo, PageQuery pageQuery) {
        Page<UserCustomerDetailVo> result = baseMapper.queryDetailPageListByUserId(pageQuery.build(),bo.getUserId());
        return TableDataInfo.build(result);
    }
}
