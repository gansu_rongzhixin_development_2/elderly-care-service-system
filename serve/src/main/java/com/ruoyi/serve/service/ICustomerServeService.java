package com.ruoyi.serve.service;

import com.ruoyi.serve.domain.CustomerServe;
import com.ruoyi.serve.domain.vo.CustomerServeVo;
import com.ruoyi.serve.domain.bo.CustomerServeBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 服务记录Service接口
 *
 * @author ruoyi
 * @date 2022-11-02
 */
public interface ICustomerServeService {

    /**
     * 查询服务记录
     */
    CustomerServeVo queryById(Long id);

    /**
     * 查询服务记录列表
     */
    TableDataInfo<CustomerServeVo> queryPageList(CustomerServeBo bo, PageQuery pageQuery);

    /**
     * 查询服务记录列表
     */
    List<CustomerServeVo> queryList(CustomerServeBo bo);

    /**
     * 新增服务记录
     */
    Boolean insertByBo(CustomerServeBo bo);

    /**
     * 修改服务记录
     */
    Boolean updateByBo(CustomerServeBo bo);

    /**
     * 校验并批量删除服务记录信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
