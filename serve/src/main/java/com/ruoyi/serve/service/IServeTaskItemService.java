package com.ruoyi.serve.service;

import com.ruoyi.serve.domain.ServeTaskItem;
import com.ruoyi.serve.domain.vo.ServeTaskItemVo;
import com.ruoyi.serve.domain.bo.ServeTaskItemBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 任务明细Service接口
 *
 * @author ruoyi
 * @date 2022-11-03
 */
public interface IServeTaskItemService {

    /**
     * 查询任务明细
     */
    ServeTaskItemVo queryById(Long id);

    /**
     * 查询任务明细列表
     */
    TableDataInfo<ServeTaskItemVo> queryPageList(ServeTaskItemBo bo, PageQuery pageQuery);

    /**
     * 查询任务明细列表
     */
    List<ServeTaskItemVo> queryList(ServeTaskItemBo bo);

    /**
     * 新增任务明细
     */
    Boolean insertByBo(ServeTaskItemBo bo);

    /**
     * 修改任务明细
     */
    Boolean updateByBo(ServeTaskItemBo bo);

    /**
     * 校验并批量删除任务明细信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    List<Map<String, Object>> groupByServiceItemId();
}
