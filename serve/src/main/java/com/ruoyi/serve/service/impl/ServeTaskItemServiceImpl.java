package com.ruoyi.serve.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.base.domain.Customer;
import com.ruoyi.base.service.ICustomerServiceItemService;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.serve.domain.bo.ServeTaskItemBo;
import com.ruoyi.serve.domain.vo.ServeTaskItemVo;
import com.ruoyi.serve.domain.ServeTaskItem;
import com.ruoyi.serve.mapper.ServeTaskItemMapper;
import com.ruoyi.serve.service.IServeTaskItemService;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * 任务明细Service业务层处理
 *
 * @author ruoyi
 * @date 2022-11-03
 */
@RequiredArgsConstructor
@Service
public class ServeTaskItemServiceImpl implements IServeTaskItemService {

    private final ServeTaskItemMapper baseMapper;

    private final ICustomerServiceItemService serviceItemService;

    /**
     * 查询任务明细
     */
    @Override
    public ServeTaskItemVo queryById(Long id){
        ServeTaskItemVo serveTaskItemVo = baseMapper.selectVoById(id);
        serveTaskItemVo.setServiceItemName(serviceItemService.queryById(serveTaskItemVo.getServiceItemId()).getItemName());
        return serveTaskItemVo;
    }

    /**
     * 查询任务明细列表
     */
    @Override
    public TableDataInfo<ServeTaskItemVo> queryPageList(ServeTaskItemBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<ServeTaskItem> lqw = buildQueryWrapper(bo);
        Page<ServeTaskItemVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        List<ServeTaskItemVo> list = result.getRecords();
        if(list != null && list.size() > 0){
            list.forEach(r->{
                r.setServiceItemName(serviceItemService.queryById(r.getServiceItemId()).getItemName());

                if(StringUtils.isNotEmpty(r.getBeforeImgs())){
                    r.setBeforeImgList(Arrays.stream(r.getBeforeImgs().split(";")).filter(s -> StringUtils.isNotEmpty(s)).collect(Collectors.toList()));
                }
                if(StringUtils.isNotEmpty(r.getAfterImgs())){
                    r.setAfterImgList(Arrays.stream(r.getAfterImgs().split(";")).filter(s -> StringUtils.isNotEmpty(s)).collect(Collectors.toList()));
                }
                if("2".equals(r.getStatus()) && r.getFinishTime()!=null && r.getSignTime()!=null){
                    r.setServeDuration( (r.getFinishTime().getTime() - r.getSignTime().getTime()) / (1000 * 60));
                }

            });
        }

        return TableDataInfo.build(result);
    }

    /**
     * 查询任务明细列表
     */
    @Override
    public List<ServeTaskItemVo> queryList(ServeTaskItemBo bo) {
        LambdaQueryWrapper<ServeTaskItem> lqw = buildQueryWrapper(bo);
        List<ServeTaskItemVo> list =  baseMapper.selectVoList(lqw);
        if(list!=null && list.size() > 0){
            list.forEach(r->{
                r.setServiceItemName(serviceItemService.queryById(r.getServiceItemId()).getItemName());
                if("2".equals(r.getStatus()) && r.getFinishTime()!=null && r.getSignTime()!=null){
                    r.setServeDuration( (r.getFinishTime().getTime() - r.getSignTime().getTime()) / (1000 * 60));
                }
            });
        }

        return list;
    }

    private LambdaQueryWrapper<ServeTaskItem> buildQueryWrapper(ServeTaskItemBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<ServeTaskItem> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getTaskId() != null, ServeTaskItem::getTaskId, bo.getTaskId());
        lqw.eq(bo.getServiceItemId() != null, ServeTaskItem::getServiceItemId, bo.getServiceItemId());
        return lqw;
    }

    /**
     * 新增任务明细
     */
    @Override
    public Boolean insertByBo(ServeTaskItemBo bo) {
        ServeTaskItem add = BeanUtil.toBean(bo, ServeTaskItem.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改任务明细
     */
    @Override
    public Boolean updateByBo(ServeTaskItemBo bo) {
        ServeTaskItem update = BeanUtil.toBean(bo, ServeTaskItem.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(ServeTaskItem entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除任务明细
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public List<Map<String, Object>> groupByServiceItemId() {
        return baseMapper.groupByServiceItemId();
    }
}
