package com.ruoyi.serve.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.serve.domain.bo.CustomerServeBo;
import com.ruoyi.serve.domain.vo.CustomerServeVo;
import com.ruoyi.serve.domain.CustomerServe;
import com.ruoyi.serve.mapper.CustomerServeMapper;
import com.ruoyi.serve.service.ICustomerServeService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 服务记录Service业务层处理
 *
 * @author ruoyi
 * @date 2022-11-02
 */
@RequiredArgsConstructor
@Service
public class CustomerServeServiceImpl implements ICustomerServeService {

    private final CustomerServeMapper baseMapper;

    /**
     * 查询服务记录
     */
    @Override
    public CustomerServeVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询服务记录列表
     */
    @Override
    public TableDataInfo<CustomerServeVo> queryPageList(CustomerServeBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<CustomerServe> lqw = buildQueryWrapper(bo);
        Page<CustomerServeVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询服务记录列表
     */
    @Override
    public List<CustomerServeVo> queryList(CustomerServeBo bo) {
        LambdaQueryWrapper<CustomerServe> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CustomerServe> buildQueryWrapper(CustomerServeBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CustomerServe> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getUserId() != null, CustomerServe::getUserId, bo.getUserId());
        lqw.eq(bo.getCustomerId() != null, CustomerServe::getCustomerId, bo.getCustomerId());
        lqw.eq(bo.getBeginTime() != null, CustomerServe::getBeginTime, bo.getBeginTime());
        lqw.eq(bo.getEndTime() != null, CustomerServe::getEndTime, bo.getEndTime());
        lqw.eq(StringUtils.isNotBlank(bo.getStatus()), CustomerServe::getStatus, bo.getStatus());
        return lqw;
    }

    /**
     * 新增服务记录
     */
    @Override
    public Boolean insertByBo(CustomerServeBo bo) {
        CustomerServe add = BeanUtil.toBean(bo, CustomerServe.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改服务记录
     */
    @Override
    public Boolean updateByBo(CustomerServeBo bo) {
        CustomerServe update = BeanUtil.toBean(bo, CustomerServe.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CustomerServe entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除服务记录
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
