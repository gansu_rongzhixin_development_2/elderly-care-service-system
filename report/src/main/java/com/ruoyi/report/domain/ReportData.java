package com.ruoyi.report.domain;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class ReportData {

    //服务区域
    @ExcelProperty("服务区域")
    private String deptName;
    //应服务次数
    @ExcelProperty("应完成")
    private Integer totalCount;

    //已完成
    @ExcelProperty("已完成")
    private Integer finishCount;

    //未开始
    @ExcelProperty("未开始")
    private Integer unbeginCount;


    //进行中
    @ExcelProperty("进行中")
    private Integer runCount;

    //已超期
    @ExcelProperty("已超期")
    private Integer expireCount;


    //服务区域
    @ExcelIgnore
    private Long deptId;

    @ExcelIgnore
    private String beginDate;


    @ExcelIgnore
    private String endDate;


}
