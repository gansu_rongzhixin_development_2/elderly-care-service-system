package com.ruoyi.report.domain;

import lombok.Data;

import java.util.List;

@Data
public class Report {
    //数据开始日期
    public String beginDate;
    //数据截止日期
    public String endDate;
    //报告名称
    public String reportName  ;
    //报告日期
    public String reportDate;
    //报告公司
    public String reportCompany = "兰州人人为老养老服务有限公司";

    public List<ReportData> dataList;

}
