package com.ruoyi.report.domain;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * 服务项目报表
 */
@Data
public class ServiceItemReport {
    //服务项目名称
    @ExcelProperty("服务项目")
    private String itemName;
    //服务次数
    @ExcelProperty("服务次数")
    private String serviceCount;

    @ExcelIgnore
    private String beginDate;

    @ExcelIgnore
    private String endDate;

}
