package com.ruoyi.report.domain;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 服务记录
 */
@Data
public class ServiceLog {
    /**
     * 客户姓名
     */
    @ExcelProperty("老人姓名")
    private String customerName;
    /**
     * 服务人员姓名
     */
    @ExcelProperty("服务人员姓名")
    private String userName;
    /**
     * 服务项目
     */
    @ExcelProperty("服务项目")
    private String serviceName;
    /**
     * 服务时间
     */
    @ExcelProperty("服务签到时间")
    private Date signTime;


    /**
     * 服务时间
     */
    @ExcelProperty("服务完成时间")
    private Date finishTime;

    /**
     * 起始时间
     */
    @ExcelIgnore
    private String beginTime;

    /**
     * 截止时间
     */
    @ExcelIgnore
    private String endTime;

    /**
     * 服务项目ID
     */
    @ExcelIgnore
    private String serviceId;

    /**
     * 老人住址
     */
    @ExcelProperty("老人住址")
    private String customerAddress;

    /**
     * 老人电话
     */
    @ExcelProperty("老人电话")
    private String customerMobile;

    /**
     * 街道ID
     */
    @ExcelIgnore
    private Long deptId;


    /**
     * 街道
     */
    @ExcelProperty("街道")
    private String deptName;

    /**
     * 查询时的月份选择条件
     */
    @ExcelIgnore
    private String month;

}
