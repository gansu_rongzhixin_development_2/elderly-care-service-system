package com.ruoyi.report.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.mapper.BaseMapperPlus;
import com.ruoyi.report.domain.ReportData;
import com.ruoyi.report.domain.ServiceItemReport;
import com.ruoyi.report.domain.ServiceLog;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 客户Mapper接口
 *
 * @author ruoyi
 * @date 2022-11-01
 */
public interface ServiceItemReportMapper extends BaseMapperPlus<ServiceItemReportMapper, ServiceItemReport, ServiceItemReport> {



    List<ServiceItemReport> selectServiceItemReportList(@Param("beginTime") String beginTime,@Param("endTime")String endTime);
}
