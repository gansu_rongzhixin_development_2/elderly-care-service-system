package com.ruoyi.report.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.mapper.BaseMapperPlus;
import com.ruoyi.report.domain.Report;
import com.ruoyi.report.domain.ReportData;
import com.ruoyi.report.domain.ServiceLog;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 客户Mapper接口
 *
 * @author ruoyi
 * @date 2022-11-01
 */
public interface ReportDataMapper extends BaseMapperPlus<ReportDataMapper, ReportData, ReportData> {

    List<ReportData> getRegionServeReportData(@Param("beginDate") String beginDate, @Param("endDate")  String endDate, @Param("status")  String status);

    Page<ServiceLog> selectServiceLogPageList(@Param("page")Page<ServiceLog> build, @Param("serviceLog")ServiceLog serviceLog);


    List<ServiceLog> selectServiceLogList(ServiceLog serviceLog);
}
