package com.ruoyi.report.service;

import com.ruoyi.base.domain.bo.CustomerBo;
import com.ruoyi.base.domain.vo.CustomerVo;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.report.domain.Report;
import com.ruoyi.report.domain.ServiceItemReport;
import com.ruoyi.report.domain.ServiceLog;

import java.util.Collection;
import java.util.List;

/**
 * 报表服务
 *
 * @author ruoyi
 * @date 2022-11-01
 */
public interface IReportService {
    /**
     * 区域服务次数报表
     * @param beginDate
     * @param endDate
     * @return
     */
   public Report getRegionServeReport(String beginDate,String endDate,String status);

    /**
     * 服务人员服务次数报表
     * @param beginDate
     * @param endDate
     * @return
     */
   public Report getServiceUserServeReport(String beginDate,String endDate);

    /**
     * 老人服务次数报表
     * @param beginDate
     * @param endDate
     * @return
     */
    public Report getCustomerServeReport(String beginDate,String endDate);

    /**
     * 查询分页服务记录
     * @param serviceLog
     * @param pageQuery
     * @return
     */
    TableDataInfo<ServiceLog> queryServiceLogPageList(ServiceLog serviceLog, PageQuery pageQuery);

    /**
     * 查询服务记录
     * @param serviceLog
     * @return
     */
    List<ServiceLog> queryServiceLogList(ServiceLog serviceLog);

    /**
     * 服务项目服务次数报表
     * @param beginDate
     * @param endDate
     * @return
     */
    public List<ServiceItemReport> getServiceItemReport(String beginDate, String endDate);
}
