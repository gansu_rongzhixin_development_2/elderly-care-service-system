package com.ruoyi.report.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.report.domain.Report;
import com.ruoyi.report.domain.ReportData;
import com.ruoyi.report.domain.ServiceItemReport;
import com.ruoyi.report.domain.ServiceLog;
import com.ruoyi.report.mapper.ReportDataMapper;
import com.ruoyi.report.mapper.ServiceItemReportMapper;
import com.ruoyi.report.service.IReportService;
import com.ruoyi.serve.domain.vo.ServeTaskVo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 报表Service业务层处理
 *
 * @author ruoyi
 * @date 2022-11-01
 */
@RequiredArgsConstructor
@Service
public class ReportServiceImpl implements IReportService {

    private final ReportDataMapper baseMapper;


    private final ServiceItemReportMapper serviceItemReportMapper;

    @Override
    public Report getRegionServeReport(String beginDate, String endDate,String status) {
        Report report = new Report();
        report.setReportName("服务次数统计报表");
        report.setBeginDate(beginDate);
        report.setEndDate(endDate);
        report.setReportDate(DateUtils.dateTimeNow("yyyy-MM-dd HH:mm:ss"));






        List<ReportData> reportData = baseMapper.getRegionServeReportData(beginDate,endDate,status);
        report.setDataList(reportData);
        return report;
    }

    @Override
    public Report getServiceUserServeReport(String beginDate, String endDate) {
        return null;
    }

    @Override
    public Report getCustomerServeReport(String beginDate, String endDate) {
        return null;
    }

    @Override
    public TableDataInfo<ServiceLog> queryServiceLogPageList(ServiceLog serviceLog, PageQuery pageQuery) {
        Page<ServiceLog> result = baseMapper.selectServiceLogPageList(pageQuery.build(),serviceLog);
        return TableDataInfo.build(result);
    }

    @Override
    public List<ServiceLog> queryServiceLogList(ServiceLog serviceLog) {
        return baseMapper.selectServiceLogList(serviceLog);
    }

    @Override
    public List<ServiceItemReport> getServiceItemReport(String beginDate, String endDate) {
        return serviceItemReportMapper.selectServiceItemReportList(beginDate,endDate);
    }
}
