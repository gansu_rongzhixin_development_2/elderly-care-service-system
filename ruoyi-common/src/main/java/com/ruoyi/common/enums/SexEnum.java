package com.ruoyi.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;


@Getter
@AllArgsConstructor
public enum SexEnum {

    /**
     * 性别
     */
    WOMAN(0, "女"),
    MAN(1, "男"),
    OTHER(2, "其它");


    private Integer code;

    private String msg;

    public static SexEnum valueOfCode(Integer code) {
        for (SexEnum sexEnum : values()) {
            if (Objects.equals(code, sexEnum.code)) {
                return sexEnum;
            }
        }
        return null;
    }

}
