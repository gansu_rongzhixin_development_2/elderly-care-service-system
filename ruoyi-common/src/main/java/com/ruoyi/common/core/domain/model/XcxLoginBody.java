package com.ruoyi.common.core.domain.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

/**
 * 短信登录对象
 *
 * @author Lion Li
 */

@Data
public class XcxLoginBody {

    @NotEmpty(message = "手机 code 不能为空")
    private String phoneCode;

//    @NotEmpty(message = "登录 code 不能为空")
    private String loginCode;

}
