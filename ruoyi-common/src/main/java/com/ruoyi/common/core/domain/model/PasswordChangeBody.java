package com.ruoyi.common.core.domain.model;

import com.ruoyi.common.constant.UserConstants;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * 用户注册对象
 *
 * @author Lion Li
 */
@Data
public class PasswordChangeBody {

    /**
     * 用户名
     */
    @NotBlank(message = "{user.rawpassword.not.blank}")
    @Length(min = UserConstants.PASSWORD_MIN_LENGTH, max = UserConstants.USERNAME_MAX_LENGTH, message = "{user.password.length.valid}")
    private String oldpassword;

    /**
     * 用户密码
     */
    @NotBlank(message = "{user.newpassword.not.blank}")
    @Length(min = UserConstants.PASSWORD_MIN_LENGTH, max = UserConstants.PASSWORD_MAX_LENGTH, message = "{user.password.length.valid}")
    private String newpassword;


}
