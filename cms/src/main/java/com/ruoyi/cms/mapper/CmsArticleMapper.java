package com.ruoyi.cms.mapper;

import com.ruoyi.cms.domain.CmsArticle;
import com.ruoyi.cms.domain.vo.CmsArticleVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;

/**
 * 文章Mapper接口
 *
 * @author ruoyi
 * @date 2022-11-04
 */
public interface CmsArticleMapper extends BaseMapperPlus<CmsArticleMapper, CmsArticle, CmsArticleVo> {

}
