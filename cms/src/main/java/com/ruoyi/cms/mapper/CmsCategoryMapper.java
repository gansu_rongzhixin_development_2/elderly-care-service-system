package com.ruoyi.cms.mapper;

import com.ruoyi.cms.domain.CmsCategory;
import com.ruoyi.cms.domain.vo.CmsCategoryVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;

/**
 * 栏目Mapper接口
 *
 * @author ruoyi
 * @date 2022-11-04
 */
public interface CmsCategoryMapper extends BaseMapperPlus<CmsCategoryMapper, CmsCategory, CmsCategoryVo> {

}
