package com.ruoyi.cms.service;

import com.ruoyi.cms.domain.CmsArticle;
import com.ruoyi.cms.domain.vo.CmsArticleVo;
import com.ruoyi.cms.domain.bo.CmsArticleBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 文章Service接口
 *
 * @author ruoyi
 * @date 2022-11-04
 */
public interface ICmsArticleService {

    /**
     * 查询文章
     */
    CmsArticleVo queryById(Long articleId);

    /**
     * 查询文章列表
     */
    TableDataInfo<CmsArticleVo> queryPageList(CmsArticleBo bo, PageQuery pageQuery);

    /**
     * 查询文章列表
     */
    List<CmsArticleVo> queryList(CmsArticleBo bo);

    /**
     * 新增文章
     */
    Boolean insertByBo(CmsArticleBo bo);

    /**
     * 修改文章
     */
    Boolean updateByBo(CmsArticleBo bo);

    /**
     * 校验并批量删除文章信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
