package com.ruoyi.cms.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.cms.domain.bo.CmsCategoryBo;
import com.ruoyi.cms.domain.vo.CmsCategoryVo;
import com.ruoyi.cms.domain.CmsCategory;
import com.ruoyi.cms.mapper.CmsCategoryMapper;
import com.ruoyi.cms.service.ICmsCategoryService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 栏目Service业务层处理
 *
 * @author ruoyi
 * @date 2022-11-04
 */
@RequiredArgsConstructor
@Service
public class CmsCategoryServiceImpl implements ICmsCategoryService {

    private final CmsCategoryMapper baseMapper;

    /**
     * 查询栏目
     */
    @Override
    public CmsCategoryVo queryById(Long categoryId){
        return baseMapper.selectVoById(categoryId);
    }

    /**
     * 查询栏目列表
     */
    @Override
    public TableDataInfo<CmsCategoryVo> queryPageList(CmsCategoryBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<CmsCategory> lqw = buildQueryWrapper(bo);
        Page<CmsCategoryVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询栏目列表
     */
    @Override
    public List<CmsCategoryVo> queryList(CmsCategoryBo bo) {
        LambdaQueryWrapper<CmsCategory> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CmsCategory> buildQueryWrapper(CmsCategoryBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CmsCategory> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getCategoryName()), CmsCategory::getCategoryName, bo.getCategoryName());
        return lqw;
    }

    /**
     * 新增栏目
     */
    @Override
    public Boolean insertByBo(CmsCategoryBo bo) {
        CmsCategory add = BeanUtil.toBean(bo, CmsCategory.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setCategoryId(add.getCategoryId());
        }
        return flag;
    }

    /**
     * 修改栏目
     */
    @Override
    public Boolean updateByBo(CmsCategoryBo bo) {
        CmsCategory update = BeanUtil.toBean(bo, CmsCategory.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CmsCategory entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除栏目
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
