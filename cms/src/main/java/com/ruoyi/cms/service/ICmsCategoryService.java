package com.ruoyi.cms.service;

import com.ruoyi.cms.domain.CmsCategory;
import com.ruoyi.cms.domain.vo.CmsCategoryVo;
import com.ruoyi.cms.domain.bo.CmsCategoryBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 栏目Service接口
 *
 * @author ruoyi
 * @date 2022-11-04
 */
public interface ICmsCategoryService {

    /**
     * 查询栏目
     */
    CmsCategoryVo queryById(Long categoryId);

    /**
     * 查询栏目列表
     */
    TableDataInfo<CmsCategoryVo> queryPageList(CmsCategoryBo bo, PageQuery pageQuery);

    /**
     * 查询栏目列表
     */
    List<CmsCategoryVo> queryList(CmsCategoryBo bo);

    /**
     * 新增栏目
     */
    Boolean insertByBo(CmsCategoryBo bo);

    /**
     * 修改栏目
     */
    Boolean updateByBo(CmsCategoryBo bo);

    /**
     * 校验并批量删除栏目信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
