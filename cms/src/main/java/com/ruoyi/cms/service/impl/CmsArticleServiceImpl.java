package com.ruoyi.cms.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.cms.domain.bo.CmsArticleBo;
import com.ruoyi.cms.domain.vo.CmsArticleVo;
import com.ruoyi.cms.domain.CmsArticle;
import com.ruoyi.cms.mapper.CmsArticleMapper;
import com.ruoyi.cms.service.ICmsArticleService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 文章Service业务层处理
 *
 * @author ruoyi
 * @date 2022-11-04
 */
@RequiredArgsConstructor
@Service
public class CmsArticleServiceImpl implements ICmsArticleService {

    private final CmsArticleMapper baseMapper;

    /**
     * 查询文章
     */
    @Override
    public CmsArticleVo queryById(Long articleId){
        return baseMapper.selectVoById(articleId);
    }

    /**
     * 查询文章列表
     */
    @Override
    public TableDataInfo<CmsArticleVo> queryPageList(CmsArticleBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<CmsArticle> lqw = buildQueryWrapper(bo);
        Page<CmsArticleVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询文章列表
     */
    @Override
    public List<CmsArticleVo> queryList(CmsArticleBo bo) {
        LambdaQueryWrapper<CmsArticle> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CmsArticle> buildQueryWrapper(CmsArticleBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CmsArticle> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getCategoryId() != null, CmsArticle::getCategoryId, bo.getCategoryId());
        lqw.like(StringUtils.isNotBlank(bo.getArticleTitle()), CmsArticle::getArticleTitle, bo.getArticleTitle());
        lqw.eq(StringUtils.isNotBlank(bo.getStatus()), CmsArticle::getStatus, bo.getStatus());
        lqw.eq(StringUtils.isNotBlank(bo.getImageUrl()), CmsArticle::getImageUrl, bo.getImageUrl());
        return lqw;
    }

    /**
     * 新增文章
     */
    @Override
    public Boolean insertByBo(CmsArticleBo bo) {
        CmsArticle add = BeanUtil.toBean(bo, CmsArticle.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setArticleId(add.getArticleId());
        }
        return flag;
    }

    /**
     * 修改文章
     */
    @Override
    public Boolean updateByBo(CmsArticleBo bo) {
        CmsArticle update = BeanUtil.toBean(bo, CmsArticle.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CmsArticle entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除文章
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
