package com.ruoyi.cms.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.annotation.ExcelDictFormat;
import com.ruoyi.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 栏目视图对象 t_cms_category
 *
 * @author ruoyi
 * @date 2022-11-04
 */
@Data
@ExcelIgnoreUnannotated
public class CmsCategoryVo {

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long categoryId;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private String categoryName;


}
