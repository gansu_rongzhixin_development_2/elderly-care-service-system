package com.ruoyi.cms.domain.bo;

import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 文章业务对象 t_cms_article
 *
 * @author ruoyi
 * @date 2022-11-04
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CmsArticleBo extends BaseEntity {

    /**
     * 
     */
    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private Long articleId;

    /**
     * 栏目
     */
    @NotNull(message = "栏目不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long categoryId;

    /**
     * 标题
     */
    @NotBlank(message = "标题不能为空", groups = { AddGroup.class, EditGroup.class })
    private String articleTitle;

    /**
     * 内容
     */
    @NotBlank(message = "内容不能为空", groups = { AddGroup.class, EditGroup.class })
    private String articleContent;

    /**
     * 文章状态（0不显示 1显示）
     */
    @NotBlank(message = "文章状态（0不显示 1显示）不能为空", groups = { AddGroup.class, EditGroup.class })
    private String status;

    /**
     * 备注
     */
    @NotBlank(message = "备注不能为空", groups = { AddGroup.class, EditGroup.class })
    private String remark;

    /**
     * 缩略图
     */
    @NotBlank(message = "缩略图不能为空", groups = { AddGroup.class, EditGroup.class })
    private String imageUrl;


}
