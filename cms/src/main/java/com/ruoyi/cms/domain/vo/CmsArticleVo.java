package com.ruoyi.cms.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.annotation.ExcelDictFormat;
import com.ruoyi.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 文章视图对象 t_cms_article
 *
 * @author ruoyi
 * @date 2022-11-04
 */
@Data
@ExcelIgnoreUnannotated
public class CmsArticleVo {

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long articleId;

    /**
     * 栏目
     */
    @ExcelProperty(value = "栏目")
    private Long categoryId;

    /**
     * 标题
     */
    @ExcelProperty(value = "标题")
    private String articleTitle;

    /**
     * 内容
     */
    @ExcelProperty(value = "内容")
    private String articleContent;

    /**
     * 文章状态（0不显示 1显示）
     */
    @ExcelProperty(value = "文章状态", converter = ExcelDictConvert.class)
    @ExcelDictFormat(readConverterExp = "0=不显示,1=显示")
    private String status;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;

    /**
     * 缩略图
     */
    @ExcelProperty(value = "缩略图")
    private String imageUrl;


}
