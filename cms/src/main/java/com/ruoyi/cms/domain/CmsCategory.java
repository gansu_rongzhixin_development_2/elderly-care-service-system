package com.ruoyi.cms.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 栏目对象 t_cms_category
 *
 * @author ruoyi
 * @date 2022-11-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_cms_category")
public class CmsCategory extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    @TableId(value = "category_id")
    private Long categoryId;
    /**
     * 
     */
    private String categoryName;

}
