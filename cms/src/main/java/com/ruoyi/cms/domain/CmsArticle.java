package com.ruoyi.cms.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 文章对象 t_cms_article
 *
 * @author ruoyi
 * @date 2022-11-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_cms_article")
public class CmsArticle extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    @TableId(value = "article_id")
    private Long articleId;
    /**
     * 栏目
     */
    private Long categoryId;
    /**
     * 标题
     */
    private String articleTitle;
    /**
     * 内容
     */
    private String articleContent;
    /**
     * 文章状态（0不显示 1显示）
     */
    private String status;
    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableLogic
    private String delFlag;
    /**
     * 备注
     */
    private String remark;
    /**
     * 缩略图
     */
    private String imageUrl;

}
