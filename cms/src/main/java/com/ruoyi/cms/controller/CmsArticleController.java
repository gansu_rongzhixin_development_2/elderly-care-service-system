package com.ruoyi.cms.controller;

import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.validate.QueryGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.cms.domain.vo.CmsArticleVo;
import com.ruoyi.cms.domain.bo.CmsArticleBo;
import com.ruoyi.cms.service.ICmsArticleService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 文章
 *
 * @author ruoyi
 * @date 2022-11-04
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/cms/cmsArticle")
public class CmsArticleController extends BaseController {

    private final ICmsArticleService iCmsArticleService;

    /**
     * 查询文章列表
     */
    @SaCheckPermission("cms:cmsArticle:list")
    @GetMapping("/list")
    public TableDataInfo<CmsArticleVo> list(CmsArticleBo bo, PageQuery pageQuery) {
        return iCmsArticleService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出文章列表
     */
    @SaCheckPermission("cms:cmsArticle:export")
    @Log(title = "文章", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CmsArticleBo bo, HttpServletResponse response) {
        List<CmsArticleVo> list = iCmsArticleService.queryList(bo);
        ExcelUtil.exportExcel(list, "文章", CmsArticleVo.class, response);
    }

    /**
     * 获取文章详细信息
     *
     * @param articleId 主键
     */
    @SaCheckPermission("cms:cmsArticle:query")
    @GetMapping("/{articleId}")
    public R<CmsArticleVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long articleId) {
        return R.ok(iCmsArticleService.queryById(articleId));
    }

    /**
     * 新增文章
     */
    @SaCheckPermission("cms:cmsArticle:add")
    @Log(title = "文章", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CmsArticleBo bo) {
        return toAjax(iCmsArticleService.insertByBo(bo) ? 1 : 0);
    }

    /**
     * 修改文章
     */
    @SaCheckPermission("cms:cmsArticle:edit")
    @Log(title = "文章", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CmsArticleBo bo) {
        return toAjax(iCmsArticleService.updateByBo(bo) ? 1 : 0);
    }

    /**
     * 删除文章
     *
     * @param articleIds 主键串
     */
    @SaCheckPermission("cms:cmsArticle:remove")
    @Log(title = "文章", businessType = BusinessType.DELETE)
    @DeleteMapping("/{articleIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] articleIds) {
        return toAjax(iCmsArticleService.deleteWithValidByIds(Arrays.asList(articleIds), true) ? 1 : 0);
    }
}
