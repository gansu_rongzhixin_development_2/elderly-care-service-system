package com.ruoyi.cms.controller;

import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.validate.QueryGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.cms.domain.vo.CmsCategoryVo;
import com.ruoyi.cms.domain.bo.CmsCategoryBo;
import com.ruoyi.cms.service.ICmsCategoryService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 栏目
 *
 * @author ruoyi
 * @date 2022-11-04
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/cms/cmsCategory")
public class CmsCategoryController extends BaseController {

    private final ICmsCategoryService iCmsCategoryService;

    /**
     * 查询栏目列表
     */
    @SaCheckPermission("cms:cmsCategory:list")
    @GetMapping("/list")
    public TableDataInfo<CmsCategoryVo> list(CmsCategoryBo bo, PageQuery pageQuery) {
        return iCmsCategoryService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出栏目列表
     */
    @SaCheckPermission("cms:cmsCategory:export")
    @Log(title = "栏目", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CmsCategoryBo bo, HttpServletResponse response) {
        List<CmsCategoryVo> list = iCmsCategoryService.queryList(bo);
        ExcelUtil.exportExcel(list, "栏目", CmsCategoryVo.class, response);
    }

    /**
     * 获取栏目详细信息
     *
     * @param categoryId 主键
     */
    @SaCheckPermission("cms:cmsCategory:query")
    @GetMapping("/{categoryId}")
    public R<CmsCategoryVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long categoryId) {
        return R.ok(iCmsCategoryService.queryById(categoryId));
    }

    /**
     * 新增栏目
     */
    @SaCheckPermission("cms:cmsCategory:add")
    @Log(title = "栏目", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CmsCategoryBo bo) {
        return toAjax(iCmsCategoryService.insertByBo(bo) ? 1 : 0);
    }

    /**
     * 修改栏目
     */
    @SaCheckPermission("cms:cmsCategory:edit")
    @Log(title = "栏目", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CmsCategoryBo bo) {
        return toAjax(iCmsCategoryService.updateByBo(bo) ? 1 : 0);
    }

    /**
     * 删除栏目
     *
     * @param categoryIds 主键串
     */
    @SaCheckPermission("cms:cmsCategory:remove")
    @Log(title = "栏目", businessType = BusinessType.DELETE)
    @DeleteMapping("/{categoryIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] categoryIds) {
        return toAjax(iCmsCategoryService.deleteWithValidByIds(Arrays.asList(categoryIds), true) ? 1 : 0);
    }
}
