# 养老服务系统

#### 介绍
智慧养老服务管理系统，针对居家养老的服务进行管理。记录老人信息，服务人员信息，按老人自理能力、失能类型按月分配服务任务，上传服务过程照片、视频、生成服务记录。

#### 软件架构
管理端基于Ruoyi-vue-plus，手机端基于Uniapp

#### 主要功能

1. 基础信息管理：老人信息管理、服务人员信息管理、服务项目管理、按失能类型配置服务项目；
2. 服务管理：老人分配、按月下发服务任务、服务项目照片/视频上传、完成服务任务；
3. 统计报表：按区域统计服务次数、服务记录查询。

#### 安装教程

1.  导入数据库
2.  启动ruoyi-admin下Application.java
3.  启动XX-job下Application.java

#### 使用说明

1.  联系13359427666

