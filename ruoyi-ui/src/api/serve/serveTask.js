import request from '@/utils/request'

// 查询服务任务列表
export function listServeTask(query) {
  return request({
    url: '/serve/serveTask/list',
    method: 'get',
    params: query
  })
}

// 查询服务任务详细
export function getServeTask(id) {
  return request({
    url: '/serve/serveTask/' + id,
    method: 'get'
  })
}

// 新增服务任务
export function addServeTask(data) {
  return request({
    url: '/serve/serveTask',
    method: 'post',
    data: data
  })
}

// 修改服务任务
export function updateServeTask(data) {
  return request({
    url: '/serve/serveTask',
    method: 'put',
    data: data
  })
}

// 删除服务任务
export function delServeTask(id) {
  return request({
    url: '/serve/serveTask/' + id,
    method: 'delete'
  })
}
