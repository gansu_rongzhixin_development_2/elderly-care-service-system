import request from '@/utils/request'

// 查询服务记录列表
export function listUserCustomer(query) {
  return request({
    url: '/serve/userCustomer/list',
    method: 'get',
    params: query
  })
}
// 查询服务老人列表
export function listUserCustomerDetail(query) {
  return request({
    url: '/serve/userCustomer/detail/list',
    method: 'get',
    params: query
  })
}
// 删除服务老人
export function delUserCustomer(id) {
  return request({
    url: '/serve/userCustomer/' + id,
    method: 'delete',
  })
}

