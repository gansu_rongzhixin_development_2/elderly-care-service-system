import request from '@/utils/request'

// 查询服务记录列表
export function listCustomerServe(query) {
  return request({
    url: '/serve/customerServe/list',
    method: 'get',
    params: query
  })
}

// 查询服务记录详细
export function getCustomerServe(id) {
  return request({
    url: '/serve/customerServe/' + id,
    method: 'get'
  })
}

// 新增服务记录
export function addCustomerServe(data) {
  return request({
    url: '/serve/customerServe',
    method: 'post',
    data: data
  })
}

// 修改服务记录
export function updateCustomerServe(data) {
  return request({
    url: '/serve/customerServe',
    method: 'put',
    data: data
  })
}

// 删除服务记录
export function delCustomerServe(id) {
  return request({
    url: '/serve/customerServe/' + id,
    method: 'delete'
  })
}
