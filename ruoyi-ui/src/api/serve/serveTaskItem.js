import request from '@/utils/request'

// 查询任务明细列表
export function listServeTaskItem(query) {
  return request({
    url: '/serve/serveTaskItem/list',
    method: 'get',
    params: query
  })
}

// 查询任务明细详细
export function getServeTaskItem(id) {
  return request({
    url: '/serve/serveTaskItem/' + id,
    method: 'get'
  })
}

// 新增任务明细
export function addServeTaskItem(data) {
  return request({
    url: '/serve/serveTaskItem',
    method: 'post',
    data: data
  })
}

// 修改任务明细
export function updateServeTaskItem(data) {
  return request({
    url: '/serve/serveTaskItem',
    method: 'put',
    data: data
  })
}

// 删除任务明细
export function delServeTaskItem(id) {
  return request({
    url: '/serve/serveTaskItem/' + id,
    method: 'delete'
  })
}
// 删除服务照片
export function deleteTaskItemImage(taskItemId,imageUrl) {
  return request({
    url: '/api/serveTask/deleteTaskImg?taskItemId=' + taskItemId + '&imgUrl=' +imageUrl,
    method: 'get'
  })
}

// 上传服务前照片
export function uploadTaskItemBeforeImage(data) {
  return request({
    url: '/api/serveTask/uploadTaskItemBeforeImage',
    method: 'post',
    data: data
  })
}

// 上传服务后照片
export function uploadTaskItemAfterImage(data) {
  return request({
    url: '/api/serveTask/uploadTaskItemAfterImage',
    method: 'post',
    data: data
  })
}
