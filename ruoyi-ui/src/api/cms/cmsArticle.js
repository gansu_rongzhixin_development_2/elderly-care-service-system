import request from '@/utils/request'

// 查询文章列表
export function listCmsArticle(query) {
  return request({
    url: '/cms/cmsArticle/list',
    method: 'get',
    params: query
  })
}

// 查询文章详细
export function getCmsArticle(articleId) {
  return request({
    url: '/cms/cmsArticle/' + articleId,
    method: 'get'
  })
}

// 新增文章
export function addCmsArticle(data) {
  return request({
    url: '/cms/cmsArticle',
    method: 'post',
    data: data
  })
}

// 修改文章
export function updateCmsArticle(data) {
  return request({
    url: '/cms/cmsArticle',
    method: 'put',
    data: data
  })
}

// 删除文章
export function delCmsArticle(articleId) {
  return request({
    url: '/cms/cmsArticle/' + articleId,
    method: 'delete'
  })
}
