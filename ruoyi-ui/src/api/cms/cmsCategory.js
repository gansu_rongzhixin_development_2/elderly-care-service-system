import request from '@/utils/request'

// 查询栏目列表
export function listCmsCategory(query) {
  return request({
    url: '/cms/cmsCategory/list',
    method: 'get',
    params: query
  })
}

// 查询栏目详细
export function getCmsCategory(categoryId) {
  return request({
    url: '/cms/cmsCategory/' + categoryId,
    method: 'get'
  })
}

// 新增栏目
export function addCmsCategory(data) {
  return request({
    url: '/cms/cmsCategory',
    method: 'post',
    data: data
  })
}

// 修改栏目
export function updateCmsCategory(data) {
  return request({
    url: '/cms/cmsCategory',
    method: 'put',
    data: data
  })
}

// 删除栏目
export function delCmsCategory(categoryId) {
  return request({
    url: '/cms/cmsCategory/' + categoryId,
    method: 'delete'
  })
}
