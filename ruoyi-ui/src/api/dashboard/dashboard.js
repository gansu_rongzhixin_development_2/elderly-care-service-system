import request from '@/utils/request'

// 查询性别数据
export function groupBySex(query) {
  return request({
    url: '/dashboard/groupBySex',
    method: 'get',
    params: query
  })
}
// 查询失能类型数据
export function groupByDisabilityType(query) {
  return request({
    url: '/dashboard/groupByDisabilityType',
    method: 'get',
    params: query
  })
}
// 查询服务项目类型数据
export function groupByServiceItemId(query) {
  return request({
    url: '/dashboard/groupByServiceItemId',
    method: 'get',
    params: query
  })
}
// 查询老人总数
export function queryCustomerCount() {
  return request({
    url: '/dashboard/getCustomerCount',
    method: 'get'
  })
}
// 查询服务人员总数
export function queryServiceUserCount() {
  return request({
    url: '/dashboard/getServiceUserCount',
    method: 'get'
  })
}
// 查询服务总次数
export function queryServiceCount() {
  return request({
    url: '/dashboard/getServiceCount',
    method: 'get'
  })
}

// 获取统计数据
export function getStatisticsData() {
  return request({
    url: '/dashboard/getStatisticsData',
    method: 'get'
  })
}
