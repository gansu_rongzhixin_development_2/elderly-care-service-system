import request from '@/utils/request'

// 查询服务项目配置列表
export function listDisabilityTypeServiceItem(query) {
  return request({
    url: '/base/disabilityTypeServiceItem/list',
    method: 'get',
    params: query
  })
}

// 查询服务项目配置详细
export function getDisabilityTypeServiceItem(id) {
  return request({
    url: '/base/disabilityTypeServiceItem/' + id,
    method: 'get'
  })
}

// 新增服务项目配置
export function addDisabilityTypeServiceItem(data) {
  return request({
    url: '/base/disabilityTypeServiceItem',
    method: 'post',
    data: data
  })
}

// 修改服务项目配置
export function updateDisabilityTypeServiceItem(data) {
  return request({
    url: '/base/disabilityTypeServiceItem',
    method: 'put',
    data: data
  })
}

// 删除服务项目配置
export function delDisabilityTypeServiceItem(id) {
  return request({
    url: '/base/disabilityTypeServiceItem/' + id,
    method: 'delete'
  })
}
