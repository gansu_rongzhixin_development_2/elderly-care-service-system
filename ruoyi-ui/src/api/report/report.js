import request from '@/utils/request'

// 查询区域服务次数列表
export function listRegionServeReportData(query) {
  return request({
    url: '/report/regionServeReportDataList',
    method: 'get',
    params: query
  })
}
//查询服务项目服务次数列表
export function listServeItemReport(query) {
  return request({
    url: '/report/serviceItemReportList',
    method: 'get',
    params: query
  })
}

