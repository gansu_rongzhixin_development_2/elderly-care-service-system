import request from '@/utils/request'


// 查询服务明细
export function listServiceLog(query) {
  return request({
    url: '/report/listServiceLog',
    method: 'get',
    params: query
  })
}
