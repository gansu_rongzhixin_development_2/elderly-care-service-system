package com.ruoyi.base.listener;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.ruoyi.base.domain.bo.CustomerBo;
import com.ruoyi.base.domain.vo.CustomerVo;
import com.ruoyi.base.service.ICustomerService;
import com.ruoyi.common.excel.ExcelListener;
import com.ruoyi.common.excel.ExcelResult;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.helper.LoginHelper;
import com.ruoyi.common.utils.ValidatorUtils;
import com.ruoyi.common.utils.spring.SpringUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * 系统用户自定义导入
 *
 * @author Lion Li
 */
@Slf4j
public class CustomerImportListener extends AnalysisEventListener<CustomerVo> implements ExcelListener<CustomerVo> {

    private final ICustomerService userService;


    private final Boolean isUpdateSupport;

    private final String operName;

    private int successNum = 0;
    private int failureNum = 0;
    private final StringBuilder successMsg = new StringBuilder();
    private final StringBuilder failureMsg = new StringBuilder();

    public CustomerImportListener(Boolean isUpdateSupport) {
        this.userService = SpringUtils.getBean(ICustomerService.class);
        this.isUpdateSupport = isUpdateSupport;
        this.operName = LoginHelper.getUsername();
    }

    @Override
    public void invoke(CustomerVo userVo, AnalysisContext context) {
        CustomerVo user = this.userService.queryByIdcard(userVo.getCustomerIdcard());
        try {
            // 验证是否存在这个用户
            if (ObjectUtil.isNull(user)) {
                CustomerBo customerBo = BeanUtil.toBean(userVo, CustomerBo.class);

                ValidatorUtils.validate(customerBo);
                customerBo.setCreateBy(operName);
                userService.insertByBo(customerBo);
                successNum++;
                successMsg.append("<br/>").append(successNum).append("、姓名 ").append(customerBo.getCustomerName()).append(" 导入成功");
            } else if (isUpdateSupport) {
                Long userId = user.getId();
                CustomerBo customerBo = BeanUtil.toBean(userVo, CustomerBo.class);
                customerBo.setId(userId);
                ValidatorUtils.validate(user);
                customerBo.setUpdateBy(operName);
                userService.updateByBo(customerBo);
                successNum++;
                successMsg.append("<br/>").append(successNum).append("、姓名 ").append(user.getCustomerName()).append(" 更新成功");
            } else {
                failureNum++;
                failureMsg.append("<br/>").append(failureNum).append("、身份证号 ").append(user.getCustomerIdcard()).append(" 已存在");
            }
        } catch (Exception e) {
            e.printStackTrace();
            failureNum++;
            String msg = "<br/>" + failureNum + "、姓名 " + user.getCustomerName() + " 导入失败：";
            failureMsg.append(msg).append(e.getMessage());
            log.error(msg, e);
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

    }

    @Override
    public ExcelResult<CustomerVo> getExcelResult() {
        return new ExcelResult<CustomerVo>() {

            @Override
            public String getAnalysis() {
                if (failureNum > 0) {
                    failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
                    throw new ServiceException(failureMsg.toString());
                } else {
                    successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
                }
                return successMsg.toString();
            }

            @Override
            public List<CustomerVo> getList() {
                return null;
            }

            @Override
            public List<String> getErrorList() {
                return null;
            }
        };
    }
}
