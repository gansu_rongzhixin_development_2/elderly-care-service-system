package com.ruoyi.base.controller;

import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.validate.QueryGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.base.domain.vo.CustomerServiceItemVo;
import com.ruoyi.base.domain.bo.CustomerServiceItemBo;
import com.ruoyi.base.service.ICustomerServiceItemService;

/**
 * 服务项目
 *
 * @author ruoyi
 * @date 2022-11-01
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/base/serviceItem")
public class CustomerServiceItemController extends BaseController {

    private final ICustomerServiceItemService iCustomerServiceItemService;

    /**
     * 查询服务项目列表
     */
    @SaCheckPermission("base:serviceItem:list")
    @GetMapping("/list")
    public R<List<CustomerServiceItemVo>> list(CustomerServiceItemBo bo) {
        List<CustomerServiceItemVo> list = iCustomerServiceItemService.queryList(bo);
        return R.ok(list);
    }

    /**
     * 导出服务项目列表
     */
    @SaCheckPermission("base:serviceItem:export")
    @Log(title = "服务项目", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CustomerServiceItemBo bo, HttpServletResponse response) {
        List<CustomerServiceItemVo> list = iCustomerServiceItemService.queryList(bo);
        ExcelUtil.exportExcel(list, "服务项目", CustomerServiceItemVo.class, response);
    }

    /**
     * 获取服务项目详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("base:serviceItem:query")
    @GetMapping("/{id}")
    public R<CustomerServiceItemVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iCustomerServiceItemService.queryById(id));
    }

    /**
     * 新增服务项目
     */
    @SaCheckPermission("base:serviceItem:add")
    @Log(title = "服务项目", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CustomerServiceItemBo bo) {
        return toAjax(iCustomerServiceItemService.insertByBo(bo) ? 1 : 0);
    }

    /**
     * 修改服务项目
     */
    @SaCheckPermission("base:serviceItem:edit")
    @Log(title = "服务项目", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CustomerServiceItemBo bo) {
        return toAjax(iCustomerServiceItemService.updateByBo(bo) ? 1 : 0);
    }

    /**
     * 删除服务项目
     *
     * @param ids 主键串
     */
    @SaCheckPermission("base:serviceItem:remove")
    @Log(title = "服务项目", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iCustomerServiceItemService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }
}
