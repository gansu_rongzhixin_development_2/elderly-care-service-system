package com.ruoyi.base.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.ruoyi.base.domain.bo.DisabilityTypeServiceItemBo;
import com.ruoyi.base.domain.vo.DisabilityTypeServiceItemVo;
import com.ruoyi.base.service.IDisabilityTypeServiceItemService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

/**
 * 失能类型-服务项目对应关系
 *
 * @author ruoyi
 * @date 2022-11-02
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/base/disabilityTypeServiceItem")
public class DisabilityTypeServiceItemController extends BaseController {

    private final IDisabilityTypeServiceItemService iDisabilityTypeServiceItemService;

    /**
     * 查询失能类型-服务项目对应关系列表
     */
    @SaCheckPermission("base:disabilityTypeServiceItem:list")
    @GetMapping("/list")
    public TableDataInfo<DisabilityTypeServiceItemVo> list(DisabilityTypeServiceItemBo bo, PageQuery pageQuery) {
        return iDisabilityTypeServiceItemService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出失能类型-服务项目对应关系列表
     */
    @SaCheckPermission("base:disabilityTypeServiceItem:export")
    @Log(title = "失能类型-服务项目对应关系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(DisabilityTypeServiceItemBo bo, HttpServletResponse response) {
        List<DisabilityTypeServiceItemVo> list = iDisabilityTypeServiceItemService.queryList(bo);
        ExcelUtil.exportExcel(list, "失能类型-服务项目对应关系", DisabilityTypeServiceItemVo.class, response);
    }

    /**
     * 获取失能类型-服务项目对应关系详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("base:disabilityTypeServiceItem:query")
    @GetMapping("/{id}")
    public R<DisabilityTypeServiceItemVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iDisabilityTypeServiceItemService.queryById(id));
    }

    /**
     * 新增失能类型-服务项目对应关系
     */
    @SaCheckPermission("base:disabilityTypeServiceItem:add")
    @Log(title = "失能类型-服务项目对应关系", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody DisabilityTypeServiceItemBo bo) {
        boolean flag = iDisabilityTypeServiceItemService.insertByBo(bo);
        return toAjax(flag ? 1 : 0);
    }

    /**
     * 修改失能类型-服务项目对应关系
     */
    @SaCheckPermission("base:disabilityTypeServiceItem:edit")
    @Log(title = "失能类型-服务项目对应关系", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody DisabilityTypeServiceItemBo bo) {
        return toAjax(iDisabilityTypeServiceItemService.updateByBo(bo) ? 1 : 0);
    }

    /**
     * 删除失能类型-服务项目对应关系
     *
     * @param ids 主键串
     */
    @SaCheckPermission("base:disabilityTypeServiceItem:remove")
    @Log(title = "失能类型-服务项目对应关系", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iDisabilityTypeServiceItemService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }
}
