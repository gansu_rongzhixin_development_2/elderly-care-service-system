package com.ruoyi.base.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import com.ruoyi.base.listener.CustomerImportListener;
import com.ruoyi.common.excel.ExcelResult;
import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.validate.QueryGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.base.domain.vo.CustomerVo;
import com.ruoyi.base.domain.bo.CustomerBo;
import com.ruoyi.base.service.ICustomerService;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 客户
 *
 * @author ruoyi
 * @date 2022-11-01
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/base/customer")
public class CustomerController extends BaseController {

    private final ICustomerService iCustomerService;

    /**
     * 查询客户列表
     */
    @SaCheckPermission("base:customer:list")
    @GetMapping("/list")
    public TableDataInfo<CustomerVo> list(CustomerBo bo, PageQuery pageQuery) {
        return iCustomerService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出客户列表
     */
    @SaCheckPermission("base:customer:export")
    @Log(title = "客户", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CustomerBo bo, HttpServletResponse response) {
        List<CustomerVo> list = iCustomerService.queryList(bo);
        ExcelUtil.exportExcel(list, "客户", CustomerVo.class, response);
    }

    /**
     * 获取客户详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("base:customer:query")
    @GetMapping("/{id}")
    public R<CustomerVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iCustomerService.queryById(id));
    }

    /**
     * 新增客户
     */
    @SaCheckPermission("base:customer:add")
    @Log(title = "客户", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CustomerBo bo) {
        return toAjax(iCustomerService.insertByBo(bo) ? 1 : 0);
    }

    /**
     * 修改客户
     */
    @SaCheckPermission("base:customer:edit")
    @Log(title = "客户", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CustomerBo bo) {
        return toAjax(iCustomerService.updateByBo(bo) ? 1 : 0);
    }


    /**
     * 分配员工
     */
    @SaCheckPermission("base:customer:edit")
    @Log(title = "客户", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping("/updateUserId")
    public R<Void> updateUserId( @RequestBody CustomerBo bo) {
        return toAjax(iCustomerService.updateUserId(bo.getUserId(),bo.getIds()) ? 1 : 0);
    }




    /**
     * 删除客户
     *
     * @param ids 主键串
     */
    @SaCheckPermission("base:customer:remove")
    @Log(title = "客户", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iCustomerService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }

    /**
     * 导入数据
     *
     * @param file          导入文件
     * @param updateSupport 是否更新已存在数据
     */
    @Log(title = "老人管理", businessType = BusinessType.IMPORT)
    @SaCheckPermission("base:customer:import")
    @PostMapping(value = "/importData", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public R<Void> importData(@RequestPart("file") MultipartFile file, boolean updateSupport) throws Exception {
        ExcelResult<CustomerVo> result = ExcelUtil.importExcel(file.getInputStream(), CustomerVo.class, new CustomerImportListener(updateSupport));
        return R.ok(result.getAnalysis());
    }

    /**
     * 获取导入模板
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) {
        ExcelUtil.exportExcel(new ArrayList<>(), "用户数据", CustomerVo.class, response);
    }

}
