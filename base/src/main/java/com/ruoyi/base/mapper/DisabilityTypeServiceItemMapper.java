package com.ruoyi.base.mapper;

import com.ruoyi.base.domain.DisabilityTypeServiceItem;
import com.ruoyi.base.domain.vo.DisabilityTypeServiceItemVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;

/**
 * 失能类型-服务项目对应关系Mapper接口
 *
 * @author ruoyi
 * @date 2022-11-02
 */
public interface DisabilityTypeServiceItemMapper extends BaseMapperPlus<DisabilityTypeServiceItemMapper, DisabilityTypeServiceItem, DisabilityTypeServiceItemVo> {

}
