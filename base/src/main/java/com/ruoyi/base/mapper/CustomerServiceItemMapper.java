package com.ruoyi.base.mapper;

import com.ruoyi.base.domain.CustomerServiceItem;
import com.ruoyi.base.domain.vo.CustomerServiceItemVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;

/**
 * 服务项目Mapper接口
 *
 * @author ruoyi
 * @date 2022-11-01
 */
public interface CustomerServiceItemMapper extends BaseMapperPlus<CustomerServiceItemMapper, CustomerServiceItem, CustomerServiceItemVo> {

}
