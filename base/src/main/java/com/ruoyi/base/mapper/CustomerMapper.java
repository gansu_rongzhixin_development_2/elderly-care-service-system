package com.ruoyi.base.mapper;

import com.ruoyi.base.domain.Customer;
import com.ruoyi.base.domain.vo.CustomerVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 客户Mapper接口
 *
 * @author ruoyi
 * @date 2022-11-01
 */
public interface CustomerMapper extends BaseMapperPlus<CustomerMapper, Customer, CustomerVo> {
    /**
     * 更新服务人员ID
     * @param userId
     * @param ids
     * @return
     */
    int updateUserId(@Param("userId") Long userId, @Param("ids") Long[] ids);

    List<Map<String, Object>> groupByDisabilityType();

    /**
     * 某月未派单的人数
     * @param beginDate
     * @param endDate
     * @return
     */
    int getMonthUnsendOrderCount(@Param("beginDate") Date beginDate,
                                 @Param("endDate")Date endDate);
}
