package com.ruoyi.base.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.base.service.ICustomerServiceItemService;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.base.domain.bo.DisabilityTypeServiceItemBo;
import com.ruoyi.base.domain.vo.DisabilityTypeServiceItemVo;
import com.ruoyi.base.domain.DisabilityTypeServiceItem;
import com.ruoyi.base.mapper.DisabilityTypeServiceItemMapper;
import com.ruoyi.base.service.IDisabilityTypeServiceItemService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 失能类型-服务项目对应关系Service业务层处理
 *
 * @author ruoyi
 * @date 2022-11-02
 */
@RequiredArgsConstructor
@Service
public class DisabilityTypeServiceItemServiceImpl implements IDisabilityTypeServiceItemService {

    private final DisabilityTypeServiceItemMapper baseMapper;


    private final ICustomerServiceItemService serviceItemService;

    /**
     * 查询失能类型-服务项目对应关系
     */
    @Override
    public DisabilityTypeServiceItemVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询失能类型-服务项目对应关系列表
     */
    @Override
    public TableDataInfo<DisabilityTypeServiceItemVo> queryPageList(DisabilityTypeServiceItemBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<DisabilityTypeServiceItem> lqw = buildQueryWrapper(bo);
        Page<DisabilityTypeServiceItemVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        result.getRecords().forEach(r->{
            r.setServiceItemName(serviceItemService.queryById(r.getServiceItemId()).getItemName());
        });
        return TableDataInfo.build(result);
    }

    /**
     * 查询失能类型-服务项目对应关系列表
     */
    @Override
    public List<DisabilityTypeServiceItemVo> queryList(DisabilityTypeServiceItemBo bo) {
        LambdaQueryWrapper<DisabilityTypeServiceItem> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<DisabilityTypeServiceItem> buildQueryWrapper(DisabilityTypeServiceItemBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<DisabilityTypeServiceItem> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getDisabilityType()), DisabilityTypeServiceItem::getDisabilityType, bo.getDisabilityType());
        lqw.eq(bo.getServiceItemId() != null, DisabilityTypeServiceItem::getServiceItemId, bo.getServiceItemId());
        return lqw;
    }

    /**
     * 新增失能类型-服务项目对应关系
     */
    @Override
    public Boolean insertByBo(DisabilityTypeServiceItemBo bo) {
        DisabilityTypeServiceItem add = BeanUtil.toBean(bo, DisabilityTypeServiceItem.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改失能类型-服务项目对应关系
     */
    @Override
    public Boolean updateByBo(DisabilityTypeServiceItemBo bo) {
        DisabilityTypeServiceItem update = BeanUtil.toBean(bo, DisabilityTypeServiceItem.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(DisabilityTypeServiceItem entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除失能类型-服务项目对应关系
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
