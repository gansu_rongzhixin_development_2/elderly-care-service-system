package com.ruoyi.base.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.common.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.base.domain.bo.CustomerServiceItemBo;
import com.ruoyi.base.domain.vo.CustomerServiceItemVo;
import com.ruoyi.base.domain.CustomerServiceItem;
import com.ruoyi.base.mapper.CustomerServiceItemMapper;
import com.ruoyi.base.service.ICustomerServiceItemService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 服务项目Service业务层处理
 *
 * @author ruoyi
 * @date 2022-11-01
 */
@RequiredArgsConstructor
@Service
public class CustomerServiceItemServiceImpl implements ICustomerServiceItemService {

    private final CustomerServiceItemMapper baseMapper;

    /**
     * 查询服务项目
     */
    @Override
    public CustomerServiceItemVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }


    /**
     * 查询服务项目列表
     */
    @Override
    public List<CustomerServiceItemVo> queryList(CustomerServiceItemBo bo) {
        LambdaQueryWrapper<CustomerServiceItem> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CustomerServiceItem> buildQueryWrapper(CustomerServiceItemBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CustomerServiceItem> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getItemName()), CustomerServiceItem::getItemName, bo.getItemName());
        lqw.eq(StringUtils.isNotBlank(bo.getItemStatus()), CustomerServiceItem::getItemStatus, bo.getItemStatus());
        return lqw;
    }

    /**
     * 新增服务项目
     */
    @Override
    public Boolean insertByBo(CustomerServiceItemBo bo) {
        CustomerServiceItem add = BeanUtil.toBean(bo, CustomerServiceItem.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改服务项目
     */
    @Override
    public Boolean updateByBo(CustomerServiceItemBo bo) {
        CustomerServiceItem update = BeanUtil.toBean(bo, CustomerServiceItem.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CustomerServiceItem entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除服务项目
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
