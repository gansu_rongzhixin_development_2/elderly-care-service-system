package com.ruoyi.base.service;

import com.ruoyi.base.domain.CustomerServiceItem;
import com.ruoyi.base.domain.vo.CustomerServiceItemVo;
import com.ruoyi.base.domain.bo.CustomerServiceItemBo;

import java.util.Collection;
import java.util.List;

/**
 * 服务项目Service接口
 *
 * @author ruoyi
 * @date 2022-11-01
 */
public interface ICustomerServiceItemService {

    /**
     * 查询服务项目
     */
    CustomerServiceItemVo queryById(Long id);


    /**
     * 查询服务项目列表
     */
    List<CustomerServiceItemVo> queryList(CustomerServiceItemBo bo);

    /**
     * 新增服务项目
     */
    Boolean insertByBo(CustomerServiceItemBo bo);

    /**
     * 修改服务项目
     */
    Boolean updateByBo(CustomerServiceItemBo bo);

    /**
     * 校验并批量删除服务项目信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
