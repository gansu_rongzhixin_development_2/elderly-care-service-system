package com.ruoyi.base.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.system.domain.vo.SysOssVo;
import com.ruoyi.system.service.ISysOssService;
import lombok.RequiredArgsConstructor;
import lombok.var;
import org.springframework.stereotype.Service;
import com.ruoyi.base.domain.bo.CustomerBo;
import com.ruoyi.base.domain.vo.CustomerVo;
import com.ruoyi.base.domain.Customer;
import com.ruoyi.base.mapper.CustomerMapper;
import com.ruoyi.base.service.ICustomerService;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 客户Service业务层处理
 *
 * @author ruoyi
 * @date 2022-11-01
 */
@RequiredArgsConstructor
@Service
public class CustomerServiceImpl implements ICustomerService {

    private final CustomerMapper baseMapper;

    private final ISysOssService ossService;
    /**
     * 查询客户
     */
    @Override
    public CustomerVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询客户列表
     */
    @Override
    public TableDataInfo<CustomerVo> queryPageList(CustomerBo bo, PageQuery pageQuery) {

        LambdaQueryWrapper<Customer> lqw = buildQueryWrapper(bo);
        Page<CustomerVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询客户列表
     */
    @Override
    public List<CustomerVo> queryList(CustomerBo bo) {

        LambdaQueryWrapper<Customer> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<Customer> buildQueryWrapper(CustomerBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<Customer> lqw = Wrappers.lambdaQuery();
        lqw.eq( Customer::getDelFlag, "0");
        lqw.eq(StringUtils.isNotBlank(bo.getCustomerNo()), Customer::getCustomerNo, bo.getCustomerNo());
        lqw.like(StringUtils.isNotBlank(bo.getCustomerName()), Customer::getCustomerName, bo.getCustomerName());
        lqw.eq(StringUtils.isNotBlank(bo.getCustomerSex()), Customer::getCustomerSex, bo.getCustomerSex());
        lqw.eq(StringUtils.isNotBlank(bo.getCustomerMobile()), Customer::getCustomerMobile, bo.getCustomerMobile());
        lqw.eq(bo.getDeptId() != null, Customer::getDeptId, bo.getDeptId());
        lqw.eq(StringUtils.isNotBlank(bo.getCustomerStatus()), Customer::getCustomerStatus, bo.getCustomerStatus());
        lqw.eq(StringUtils.isNotBlank(bo.getDisabilityType())&&!"-1".equals(bo.getDisabilityType()), Customer::getDisabilityType, bo.getDisabilityType());
        lqw.eq(bo.getUserId()!=null, Customer::getUserId, bo.getUserId());
        lqw.eq( StringUtils.isNotBlank(bo.getSendOrder()), Customer::getSendOrder, bo.getSendOrder());
        return lqw;
    }

    /**
     * 新增客户
     */
    @Override
    public Boolean insertByBo(CustomerBo bo) {
        //设置头像为云存储的url
        if(StringUtils.isNotEmpty(bo.getCustomerImg())){
            SysOssVo sysOssVo = ossService.getById(Long.parseLong(bo.getCustomerImg()));
            if(sysOssVo!=null){
                bo.setCustomerImg(sysOssVo.getUrl());
            }
        }


        Customer add = BeanUtil.toBean(bo, Customer.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改客户
     */
    @Override
    public Boolean updateByBo(CustomerBo bo) {
        //设置头像为云存储的url
        /*if(StringUtils.isNotEmpty(bo.getCustomerImg())){
            SysOssVo sysOssVo = ossService.getById(Long.parseLong(bo.getCustomerImg()));
            if(sysOssVo!=null){
                bo.setCustomerImg(sysOssVo.getUrl());
            }
        }*/
        Customer update = BeanUtil.toBean(bo, Customer.class);
        if("1".equals(bo.getCustomerStatus())){
            // 如果身故，那么不派单
            update.setSendOrder("1");
        }
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(Customer entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除客户
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        /*if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;*/
        int count = 0;
        List<Customer> customerList = baseMapper.selectBatchIds(ids);
        for(int i = 0 ; i < customerList.size(); i++){
            Customer update = customerList.get(i);
            update.setDelFlag("1");
            int row = baseMapper.updateById(update);
            if(row > 0){
                count++;
            }
        }

        return count > 0;

    }

    @Override
    public CustomerVo queryByIdcard(String customerIdcard) {

        LambdaQueryWrapper<Customer> lqw = Wrappers.lambdaQuery();
        lqw.eq( Customer::getCustomerIdcard, customerIdcard);
        lqw.eq( Customer::getDelFlag, "0");

        return baseMapper.selectVoOne(lqw);
    }

    @Override
    public Boolean updateUserId(Long userId, Long[] ids) {
        return baseMapper.updateUserId(userId,ids)>0;
    }

    @Override
    public List<Map<String, Object>> groupBySex() {
        QueryWrapper<Customer> lqw = new QueryWrapper<Customer>();
        lqw.select("count(*) as count,customer_sex");
        lqw.eq("del_flag","0");
        lqw.groupBy("customer_sex");
        return baseMapper.selectMaps(lqw);
    }

    @Override
    public List<Map<String, Object>> groupByDisabilityType() {
        return baseMapper.groupByDisabilityType();
    }

    @Override
    public int getCustomerCount() {
        LambdaQueryWrapper  wrapper = new LambdaQueryWrapper <Customer>()
                .eq(Customer::getDelFlag, "0");
        return baseMapper.selectCount(wrapper).intValue();
    }

    @Override
    public int getMonthUnsendOrderCount(String month) {
        var localDate = LocalDate.parse(month + "01", DateTimeFormatter.BASIC_ISO_DATE);
        ZoneId zone = ZoneId.systemDefault();
        Date beginDate = Date.from(localDate.with(TemporalAdjusters.firstDayOfMonth()).atStartOfDay().atZone(zone).toInstant());
        Date endDate = Date.from(localDate.with(TemporalAdjusters.lastDayOfMonth()).atStartOfDay().atZone(zone).toInstant());
        return baseMapper.getMonthUnsendOrderCount(beginDate, endDate);
    }

}
