package com.ruoyi.base.service;

import com.ruoyi.base.domain.Customer;
import com.ruoyi.base.domain.vo.CustomerVo;
import com.ruoyi.base.domain.bo.CustomerBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 客户Service接口
 *
 * @author ruoyi
 * @date 2022-11-01
 */
public interface ICustomerService {

    /**
     * 查询客户
     */
    CustomerVo queryById(Long id);

    /**
     * 查询客户列表
     */
    TableDataInfo<CustomerVo> queryPageList(CustomerBo bo, PageQuery pageQuery);

    /**
     * 查询客户列表
     */
    List<CustomerVo> queryList(CustomerBo bo);

    /**
     * 新增客户
     */
    Boolean insertByBo(CustomerBo bo);

    /**
     * 修改客户
     */
    Boolean updateByBo(CustomerBo bo);

    /**
     * 校验并批量删除客户信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * 根据身份证号码查询
     * @param customerIdcard
     * @return
     */
    CustomerVo queryByIdcard(String customerIdcard);

    /**
     * 更新服务的员工
     * @param userId
     * @param ids
     * @return
     */
    Boolean updateUserId(Long userId, Long[] ids);

    /**
     * 性别分组数据
     * @return
     */
    List<Map<String, Object>> groupBySex();

    /**
     * 失能类型分组数据
     * @return
     */
    List<Map<String, Object>> groupByDisabilityType();

    /**
     * 获取老人总数
     * @return
     */
    int getCustomerCount();

    /**
     * 获取某月未派单人数
     * @param month
     * @return
     */
    int getMonthUnsendOrderCount(String month);


}
