package com.ruoyi.base.service;

import com.ruoyi.base.domain.DisabilityTypeServiceItem;
import com.ruoyi.base.domain.vo.DisabilityTypeServiceItemVo;
import com.ruoyi.base.domain.bo.DisabilityTypeServiceItemBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 失能类型-服务项目对应关系Service接口
 *
 * @author ruoyi
 * @date 2022-11-02
 */
public interface IDisabilityTypeServiceItemService {

    /**
     * 查询失能类型-服务项目对应关系
     */
    DisabilityTypeServiceItemVo queryById(Long id);

    /**
     * 查询失能类型-服务项目对应关系列表
     */
    TableDataInfo<DisabilityTypeServiceItemVo> queryPageList(DisabilityTypeServiceItemBo bo, PageQuery pageQuery);

    /**
     * 查询失能类型-服务项目对应关系列表
     */
    List<DisabilityTypeServiceItemVo> queryList(DisabilityTypeServiceItemBo bo);

    /**
     * 新增失能类型-服务项目对应关系
     */
    Boolean insertByBo(DisabilityTypeServiceItemBo bo);

    /**
     * 修改失能类型-服务项目对应关系
     */
    Boolean updateByBo(DisabilityTypeServiceItemBo bo);

    /**
     * 校验并批量删除失能类型-服务项目对应关系信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
