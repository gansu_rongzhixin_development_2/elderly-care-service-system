package com.ruoyi.base.domain.bo;

import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 失能类型-服务项目对应关系业务对象 t_disability_type_service_item
 *
 * @author ruoyi
 * @date 2022-11-02
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class DisabilityTypeServiceItemBo extends BaseEntity {

    /**
     *
     */
    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 失能类型
     */
    @NotBlank(message = "失能类型不能为空", groups = { AddGroup.class, EditGroup.class })
    private String disabilityType;

    /**
     * 服务项目
     */
    private Long serviceItemId;



    /**
     * 服务次数
     */
    private Integer serviceTimes;


}
