package com.ruoyi.base.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.annotation.ExcelDictFormat;
import com.ruoyi.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 失能类型-服务项目对应关系视图对象 t_disability_type_service_item
 *
 * @author ruoyi
 * @date 2022-11-02
 */
@Data
@ExcelIgnoreUnannotated
public class DisabilityTypeServiceItemVo {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    private Long id;

    /**
     * 失能类型
     */
    private String disabilityType;

    /**
     * 服务项目
     */
    private Long serviceItemId;

    /**
     * 服务项目
     */
    private Integer serviceTimes;

    /**
     * 服务次数
     */
    private String serviceItemName;


}
