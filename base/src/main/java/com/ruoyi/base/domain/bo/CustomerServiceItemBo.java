package com.ruoyi.base.domain.bo;

import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import java.math.BigDecimal;
import com.ruoyi.common.core.domain.TreeEntity;

/**
 * 服务项目业务对象  t_service_item
 *
 * @author ruoyi
 * @date 2022-11-01
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CustomerServiceItemBo extends TreeEntity<CustomerServiceItemBo> {

    /**
     * 服务项目ID
     */
    @NotNull(message = "服务项目ID不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 项目名称
     */
    @NotBlank(message = "项目名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String itemName;

    /**
     * 父级项目
     */
    private Long parentId;

    /**
     * 项目状态：0=启用；1=停用
     */
    @NotBlank(message = "项目状态：0=启用；1=停用不能为空", groups = { AddGroup.class, EditGroup.class })
    private String itemStatus;

    /**
     * 服务价格
     */
    private BigDecimal  itemPrice;

    /**
     * 计费标准
     */
    private String itemUnit;

    /**
     * 服务项目介绍
     */
    @NotBlank(message = "服务项目介绍不能为空", groups = { AddGroup.class, EditGroup.class })
    private String itemDescription;


}
