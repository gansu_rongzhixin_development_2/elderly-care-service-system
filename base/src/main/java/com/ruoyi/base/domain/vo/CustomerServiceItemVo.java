package com.ruoyi.base.domain.vo;

import java.math.BigDecimal;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.annotation.ExcelDictFormat;
import com.ruoyi.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 服务项目视图对象  t_service_item
 *
 * @author ruoyi
 * @date 2022-11-01
 */
@Data
@ExcelIgnoreUnannotated
public class CustomerServiceItemVo {

    private static final long serialVersionUID = 1L;

    /**
     * 服务项目ID
     */
    @ExcelProperty(value = "服务项目ID")
    private Long id;

    /**
     * 项目名称
     */
    @ExcelProperty(value = "项目名称")
    private String itemName;

    /**
     * 父级项目
     */
    @ExcelProperty(value = "父级项目")
    private Long parentId;

    /**
     * 项目状态：0=启用；1=停用
     */
    @ExcelProperty(value = "项目状态：0=启用；1=停用", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "service_item_status")
    private String itemStatus;

    /**
     * 服务价格
     */
    @ExcelProperty(value = "服务价格")
    private BigDecimal  itemPrice;

    /**
     * 计费标准
     */
    @ExcelProperty(value = "计费标准", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "service_item_unit")
    private String itemUnit;

    /**
     * 服务项目介绍
     */
    @ExcelProperty(value = "服务项目介绍")
    private String itemDescription;


}
