package com.ruoyi.base.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import java.math.BigDecimal;
import com.ruoyi.common.core.domain.TreeEntity;

/**
 * 服务项目对象  t_service_item
 *
 * @author ruoyi
 * @date 2022-11-01
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(" t_service_item")
public class CustomerServiceItem extends TreeEntity<CustomerServiceItem> {

    private static final long serialVersionUID=1L;

    /**
     * 服务项目ID
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 项目名称
     */
    private String itemName;
    /**
     * 父级项目
     */
    private Long parentId;
    /**
     * 项目状态：0=启用；1=停用
     */
    private String itemStatus;
    /**
     * 服务价格
     */
    private BigDecimal  itemPrice;
    /**
     * 计费标准
     */
    private String itemUnit;
    /**
     * 服务项目介绍
     */
    private String itemDescription;

}
