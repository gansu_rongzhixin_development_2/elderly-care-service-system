package com.ruoyi.base.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 失能类型-服务项目对应关系对象 t_disability_type_service_item
 *
 * @author ruoyi
 * @date 2022-11-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_disability_type_service_item")
public class DisabilityTypeServiceItem extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 失能类型
     */
    private String disabilityType;
    /**
     * 服务项目
     */
    private Long serviceItemId;


    /**
     * 服务次数
     */
    private Integer serviceTimes;

}
