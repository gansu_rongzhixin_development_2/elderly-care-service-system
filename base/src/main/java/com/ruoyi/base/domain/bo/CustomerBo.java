package com.ruoyi.base.domain.bo;

import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.utils.IdCardUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 客户业务对象 t_customer
 *
 * @author ruoyi
 * @date 2022-11-01
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CustomerBo extends BaseEntity {

    /**
     * 顾客ID
     */
    @NotNull(message = "顾客ID不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 多个顾客ID
     */
    private Long[] ids;


    /**
     * 服务人员ID
     */
    private Long userId;

    /**
     * 编号
     */
//    @NotBlank(message = "编号不能为空", groups = { AddGroup.class, EditGroup.class })
    private String customerNo;

    /**
     * 姓名
     */
    @NotBlank(message = "姓名不能为空", groups = { AddGroup.class, EditGroup.class })
    private String customerName;

    /**
     * 性别
     */
    @NotBlank(message = "性别不能为空", groups = { AddGroup.class, EditGroup.class })
    private String customerSex;

    /**
     * 身份证
     */
    @NotBlank(message = "身份证不能为空", groups = { AddGroup.class, EditGroup.class })
    private String customerIdcard;

    /**
     * 照片
     */
//    @NotBlank(message = "照片不能为空", groups = { AddGroup.class, EditGroup.class })
    private String customerImg;

    /**
     * 电话
     */
    @NotBlank(message = "电话不能为空", groups = { AddGroup.class, EditGroup.class })
    private String customerMobile;

    /**
     * 省
     */
    private String customerProvince;

    /**
     * 市
     */
    private String customerCity;

    /**
     * 县
     */
    private String customerCounty;

    /**
     * 详细地址
     */
    @NotBlank(message = "详细地址不能为空", groups = { AddGroup.class, EditGroup.class })
    private String customerAddress;

    /**
     * 所属服务点
     */
    @NotNull(message = "所属服务点不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long deptId;

    /**
     * 状态：见字典表，0=正常；1=停用
     */
    private String customerStatus;

    /**
     * 病史
     */
    private String customerMedicalHistory;

    /**
     * 注意事项
     */
    private String customerAttention;

    /**
     * 失能类型
     */
    @NotBlank(message = "失能类型不能为空", groups = { AddGroup.class, EditGroup.class })
    private String disabilityType;

    /**
     * 年龄
     */
    private Integer customerAge;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否派单：0=派单，1=不排单
     */
    private String sendOrder;

    /**
     * 删除状态：0=未删除，1=已删除
     */
    private String delFlag;




}
