package com.ruoyi.base.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.annotation.ExcelDictFormat;
import com.ruoyi.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 客户视图对象 t_customer
 *
 * @author ruoyi
 * @date 2022-11-01
 */
@Data
@ExcelIgnoreUnannotated
public class CustomerVo {

    private static final long serialVersionUID = 1L;

    /**
     * 顾客ID
     */
    private Long id;

    /**
     * 编号
     */
    @ExcelProperty(value = "编号")
    private String customerNo;

    /**
     * 姓名
     */
    @ExcelProperty(value = "姓名")
    private String customerName;

    /**
     * 性别
     */
    @ExcelProperty(value = "性别", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_user_sex")
    private String customerSex;

    /**
     * 身份证
     */
    @ExcelProperty(value = "身份证")
    private String customerIdcard;

    /**
     * 照片
     */
    private String customerImg;

    /**
     * 手机号
     */
    @ExcelProperty(value = "手机号")
    private String customerMobile;

    /**
     * 省
     */
    private String customerProvince;

    /**
     * 市
     */
    private String customerCity;

    /**
     * 县
     */
    private String customerCounty;

    /**
     * 详细地址
     */
    @ExcelProperty(value = "详细地址")
    private String customerAddress;

    /**
     * 所属服务点
     */
    @ExcelProperty(value = "街道")
    private Long deptId;

    /**
     * 状态：见字典表，0=正常；1=停用
     */
    private String customerStatus;

    /**
     * 病史
     */
    @ExcelProperty(value = "病史")
    private String customerMedicalHistory;

    /**
     * 注意事项
     */
    @ExcelProperty(value = "注意事项")
    private String customerAttention;

    /**
     * 失能类型
     */
    @ExcelProperty(value = "失能类型", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "disability_type")
    private String disabilityType;

    /**
     * 年龄
     */
    @ExcelProperty(value = "年龄")
    private Long customerAge;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;

    /**
     * 是否派单：0=派单，1=不排单
     */
    private String sendOrder;

}
