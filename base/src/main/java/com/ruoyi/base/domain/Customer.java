package com.ruoyi.base.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 客户对象 t_customer
 *
 * @author ruoyi
 * @date 2022-11-01
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_customer")
public class Customer extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 顾客ID
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 服务人员ID
     */
    private Long userId;
    /**
     * 编号
     */
    private String customerNo;
    /**
     * 姓名
     */
    private String customerName;
    /**
     * 性别
     */
    private String customerSex;
    /**
     * 身份证
     */
    private String customerIdcard;
    /**
     * 照片
     */
    private String customerImg;
    /**
     * 手机号
     */
    private String customerMobile;
    /**
     * 省
     */
    private String customerProvince;
    /**
     * 市
     */
    private String customerCity;
    /**
     * 县
     */
    private String customerCounty;
    /**
     * 详细地址
     */
    private String customerAddress;
    /**
     * 所属服务点
     */
    private Long deptId;
    /**
     * 状态：见字典表，0=正常；1=停用
     */
    private String customerStatus;
    /**
     * 病史
     */
    private String customerMedicalHistory;
    /**
     * 注意事项
     */
    private String customerAttention;
    /**
     * 失能类型
     */
    private String disabilityType;
    /**
     * 年龄
     */
    private Long customerAge;
    /**
     * 备注
     */
    private String remark;

    /**
     * 是否派单：0=派单，1=不排单
     */
    private String sendOrder;

    /**
     * 删除状态：0=未删除，1=已删除
     */
    private String delFlag;

}
