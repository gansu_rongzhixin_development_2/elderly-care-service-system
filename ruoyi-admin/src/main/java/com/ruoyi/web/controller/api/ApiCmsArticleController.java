package com.ruoyi.web.controller.api;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaIgnore;
import com.ruoyi.cms.domain.bo.CmsArticleBo;
import com.ruoyi.cms.domain.vo.CmsArticleVo;
import com.ruoyi.cms.service.ICmsArticleService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

/**
 * 文章
 *
 * @author ruoyi
 * @date 2022-11-04
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/cms/")
public class ApiCmsArticleController extends BaseController {

    private final ICmsArticleService iCmsArticleService;

    /**
     * 查询文章列表
     */
    @SaIgnore
    @GetMapping("/article/list")
    public TableDataInfo<CmsArticleVo> list(CmsArticleBo bo, PageQuery pageQuery) {
        return iCmsArticleService.queryPageList(bo, pageQuery);
    }


    /**
     * 获取文章详细信息
     *
     * @param articleId 主键
     */
    @SaIgnore
    @GetMapping("/article/{articleId}")
    public R<CmsArticleVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long articleId) {
        return R.ok(iCmsArticleService.queryById(articleId));
    }


}
