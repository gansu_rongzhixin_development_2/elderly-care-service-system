package com.ruoyi.web.controller.api;


import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.secure.BCrypt;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ArrayUtil;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.domain.model.PasswordChangeBody;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.helper.LoginHelper;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.MimeTypeUtils;
import com.ruoyi.system.domain.SysOss;
import com.ruoyi.system.service.ISysDeptService;
import com.ruoyi.system.service.ISysOssService;
import com.ruoyi.system.service.ISysUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 文件上传 控制层
 *
 * @author Lion Li
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/street")
public class ApiStreetController extends BaseController {

    private final ISysDeptService deptService;

    /**
     * 查询街道列表
     */
    @GetMapping("/list")
    public R<List<SysDept>> list() {
        LoginUser loginUser = LoginHelper.getLoginUser();
        //如果不是任秀霞的账号，那么只查询自己客户所在的街道
        if(loginUser.getUserId()!=1621399601530609665l){
            List<SysDept> depts = deptService.selectUserDeptList(loginUser.getUserId());
            return R.ok(depts);
        }else{
            List<SysDept> depts = deptService.selectDeptList(new SysDept());
            return R.ok(depts);
        }
    }
}
