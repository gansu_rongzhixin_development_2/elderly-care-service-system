package com.ruoyi.web.controller.api;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ruoyi.base.domain.bo.CustomerBo;
import com.ruoyi.base.domain.vo.CustomerVo;
import com.ruoyi.base.service.ICustomerService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.domain.entity.SysDictData;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.helper.LoginHelper;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.serve.domain.bo.ServeTaskBo;
import com.ruoyi.serve.domain.bo.ServeTaskItemBo;
import com.ruoyi.serve.domain.vo.ServeTaskItemVo;
import com.ruoyi.serve.domain.vo.ServeTaskVo;
import com.ruoyi.serve.service.IServeTaskItemService;
import com.ruoyi.serve.service.IServeTaskService;
import com.ruoyi.system.domain.SysOss;
import com.ruoyi.system.service.ISysDictTypeService;
import com.ruoyi.system.service.ISysOssService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

/**
 * 服务任务
 *
 * @author ruoyi
 * @date 2022-11-03
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/customer")
public class ApiCustomerController extends BaseController {

    private final ICustomerService customerService;

    private final ISysDictTypeService dictTypeService;

    /**
     * 查询客户列表
     */
    @GetMapping("/list")
    public TableDataInfo<CustomerVo> list(CustomerBo bo, PageQuery pageQuery) {
        LoginUser loginUser = LoginHelper.getLoginUser();
        bo.setUserId(loginUser.getUserId());
        return customerService.queryPageList(bo, pageQuery);
    }

    /**
     * 查询客户详情
     */
    @GetMapping("/getCustomerInfo")
    public R<CustomerVo> getCustomerInfo(Long id) {
        return R.ok(customerService.queryById(id));
    }



    /**
     * 查询客户详情
     */
    @GetMapping("/listDisabilityType")
    public R<List<SysDictData>> listDisabilityType() {
        String dictType = "disability_type";
        List<SysDictData> data = dictTypeService.selectDictDataByType(dictType);
        if (ObjectUtil.isNull(data)) {
            data = new ArrayList<>();
        }
        return R.ok(data);
    }




}
