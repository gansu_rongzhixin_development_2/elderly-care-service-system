package com.ruoyi.web.controller.dashboard;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.util.ArrayUtil;
import com.ruoyi.base.service.ICustomerService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.serve.service.IServeTaskItemService;
import com.ruoyi.serve.service.IServeTaskService;
import com.ruoyi.system.service.ISysDeptService;
import com.ruoyi.system.service.ISysUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 部门信息
 *
 * @author Lion Li
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/dashboard")
public class DashboardController extends BaseController {

    private final ICustomerService customerService;

    private final IServeTaskItemService taskItemService;

    private final ISysUserService userService;

    private final IServeTaskService taskService;

    /**
     * 获取性别数据
     */
    @GetMapping("/groupBySex")
    public R groupBySex() {
        List<Map<String, Object>> data = customerService.groupBySex();
        return R.ok(data);
    }

    /**
     * 获取失能类型数据
     */
    @GetMapping("/groupByDisabilityType")
    public R groupByDisabilityType() {
        List<Map<String, Object>> data = customerService.groupByDisabilityType();
        return R.ok(data);
    }

    /**
     * 获取服务项目数据
     */
    @GetMapping("/groupByServiceItemId")
    public R groupByServiceItemId() {
        List<Map<String, Object>> data = taskItemService.groupByServiceItemId();
        return R.ok(data);
    }

    /**
     * 获取老人总数
     */
    @GetMapping("/getCustomerCount")
    public R getCustomerCount() {
        int count = customerService.getCustomerCount();
        return R.ok(count);
    }


    /**
     * 获取服务人员总数
     */
    @GetMapping("/getServiceUserCount")
    public R getServiceUserCount() {
        int count = userService.getServiceUserCount();
        return R.ok(count);
    }


    /**
     * 获取总服务次数
     */
    @GetMapping("/getServiceCount")
    public R getServiceCount() {
        int count = taskService.getServiceCount();
        return R.ok(count);
    }



    /**
     * 获取老人总数、服务员人数、服务次数、本月未派单人数、本月派单已服务人数、本月派单未服务人数
     */
    @GetMapping("/getStatisticsData")
    public R getStatisticsData() {
        int customerCount = customerService.getCustomerCount();
        int serviceUserCount = userService.getServiceUserCount();
        int serviceCount = taskService.getServiceCount();


        String month = DateUtils.dateTimeNow("yyyyMM");
        int monthUnsendOrderCount = customerService.getMonthUnsendOrderCount(month);
        int monthServicedCount = taskService.getMonthServicedCount(month);
        int monthUnservicedCount = taskService.getMonthUnservicedCount(month);

        Map<String,Integer> result = new HashMap<String,Integer>();
        result.put("customerCount", customerCount);
        result.put("serviceUserCount", serviceUserCount);
        result.put("serviceCount", serviceCount);
        result.put("monthUnsendOrderCount", monthUnsendOrderCount);
        result.put("monthServicedCount", monthServicedCount);
        result.put("monthUnservicedCount", monthUnservicedCount);


        return R.ok(result);
    }


}
